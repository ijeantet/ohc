import sys
import os
import logging
import time
from sklearn.metrics.pairwise import pairwise_distances
from scipy.cluster.hierarchy import linkage
import hdbscan
import pandas as pd
import numpy as np
# Local files
sys.path.insert(0, '..')
import lib.utils as utils
import lib.clustering as clustering

# General configuration
# Pass logging.INFO/logging.CRITICAL as level to display INFO or CRITICAL events
logger = utils.log_config(level=logging.INFO)

print("Configuration done")
print("------------------")

#################
# Preprocessing #
#################

log_path = "./results/execution_time"
# Create the log directory if necessary
if not os.path.exists(log_path):
    os.makedirs(log_path)

# Init logs
log_name = "test_execution_time"

##################
# OHC parameters #
##################

voc_step = 100
batch_size = 1
met = "euclidean"
voc_size = voc_step
data_size = 2           # 2-dimensional vectors
low = 0
high = 100000

# If you want you can change the following parameters
nb_repetitions = 5                                  # Number of repetitions per vocab size
time_limit = 100                                    # Time limit (in second) given to the OHC algo to compute the quasi-dendrogram
voc_max = 3000                                      # Maximal size of the vocabulary to consider
li = reversed([0, 0.001, 0.005, 0.01, 0.2, 0.5])    # List of merging criteria to explore

###################
# Data parameters #
###################

dataset = "random"

# hierarchy_path = "./hierarchies/" + dataset + "/" + str(data_size)
log_name += ".data_" + dataset

##############
# Other init #
##############

# Log file name
ts = time.gmtime()
log_name += "." + time.strftime("%s", ts) + ".csv"

# Construct the path of the log file
log_name = os.path.join(log_path, log_name)

#################
# OHC algorithm #
#################

for l in li:
    
    results = dict()
    
    name = 'random'
    results["merging_criterion"] = l

    voc_size = voc_step
    limit_exceeded = False
    nb_rep = 0
    while voc_size <= voc_max and not limit_exceeded:
        nb_rep += 1
        # Generate the data
        df_model = pd.DataFrame(np.random.randint(low, high, size=(voc_size, data_size)))
        print(str(voc_size) + " vectors of " + str(data_size) + " dimensions generated")
        results["voc_size"] = voc_size

        # Compute or load the distance matrix if existing
        distance_matrix = pairwise_distances(df_model, metric=met)
        # From the distance matrix to a sorted list of tuples
        li = utils.matrix_to_tuples(distance_matrix, triangle=True)
        sorted_tuples = sorted(li, key=utils.tuple_distance)
        # Normalize the distances
        min_dist = 0
        norm_factor = utils.tuple_distance(sorted_tuples[-1]) - min_dist
        sorted_tuples = [(a, b, float(d-min_dist)/norm_factor) for (a, b, d) in sorted_tuples]

        # Compute the known algorithm hierarchies as reference
        time_start = time.process_time()
        slink_matrix = linkage(df_model.values, method='single', metric=met)  # For Slink the complexity is in O(n^2)
        time_slink = time.process_time() - time_start
        results["time_slink"] = time_slink
        print("slink executed in " + str(time_slink) + "s")
        time_start = time.process_time()
        average_matrix = linkage(df_model.values, method='average', metric=met)  # For Slink the complexity is in O(n^2)
        time_average = time.process_time() - time_start
        results["time_average"] = time_average
        print("average executed in " + str(time_average) + "s")
        time_start = time.process_time()
        ward_matrix = linkage(df_model.values, method='ward', metric=met)  # Complexity in O(n^2) time and space
        time_ward = time.process_time() - time_start
        results["time_ward"] = time_ward
        print("ward executed in " + str(time_ward) + "s")
        time_start = time.process_time()
        clusterer = hdbscan.HDBSCAN(min_cluster_size=2, metric='precomputed')
        clusterer.fit(distance_matrix)
        hdbscan_matrix = clusterer.single_linkage_tree_.to_numpy()
        time_hdbscan = time.process_time() - time_start
        results["time_hdbscan"] = time_hdbscan
        print("hdbscan executed in " + str(time_hdbscan) + "s")

        print("merging criterion = " + str(l) + ", batch size = " + str(batch_size))
        logs = dict()
        # Run the algo for a given merging criterion
        ohc_hierarchy = clustering.hierarchical_clustering_with_overlapping(sorted_tuples, merging_criterion=l,
                                                                            batch_size=batch_size, normalize=True,
                                                                            name=name, time_limit=time_limit, logs=logs)
        if logs["time"] == -1:
            logs["time"] = "inf"
            print("OHC time limit exceeded")
            limit_exceeded = True

        if not limit_exceeded:
            results["nb_tuples"] = logs["nb_tuples"]
            results["time_ohc"] = logs["time"]

            # Uncomment to save the produced quasi-dendrograms
            # if hierarchy_path and nb_rep == 1:
            #    utils.save_to_pickle(hierarchy_path + "/" + str(voc_size), ohc_hierarchy)
            #    del ohc_hierarchy

            # If the file doesn't exist we need to add the csv header
            if not os.path.isfile(log_name):
                with open(log_name, "w+") as log_file:
                    header = ';'.join(list(results.keys())) + "\n"
                    log_file.write(header)
            # Write the line in the file
            with open(log_name, "a+") as log_file:
                log_file.write(';'.join([str(v) for v in list(results.values())]).replace('.', ',') + "\n")

            if nb_rep == nb_repetitions:
                voc_size += voc_step
                nb_rep = 0

print('Logs saved in ' + log_name)
