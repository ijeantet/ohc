import sys
import os
import logging
import time
from sklearn.metrics.pairwise import pairwise_distances
from scipy.cluster.hierarchy import linkage
import hdbscan
import pandas as pd
import numpy as np
# Local files
sys.path.insert(0, '..')
import lib.utils as utils
import lib.clustering as clustering
import lib.metric as metric

# General configuration
# Pass logging.INFO/logging.CRITICAL as level to display INFO or CRITICAL events
logger = utils.log_config(level=logging.INFO)

print("Configuration done")
print("------------------")

#################
# Preprocessing #
#################

log_path = "./results/ohc_similarity"
# Create the log directory if necessary
if not os.path.exists(log_path):
    os.makedirs(log_path)

# Init logs
log_name = "test_ohc_similarity"

##################
# OHC parameters #
##################

l_min = 0.001
l_max = 1.0
l_step = 10
batch_size = 1
time_limit = 1000
met = "euclidean"
data_size = 2   # 2-dimensional vectors
low = 0
high = 100000

# If you want you can change the following parameters
nb_repetitions = 5  # Number of repetitions of the process
voc_size = 200      # Number of vectors on which to compute the hierarchies

###################
# Data parameters #
###################

dataset = "random"
log_name += ".data_" + dataset + ".vocab_" + str(voc_size)

##############
# Other init #
##############

# Log file name
ts = time.gmtime()
log_name += "." + time.strftime("%s", ts) + ".csv"

# Construct the path of the log file
log_name = os.path.join(log_path, log_name)

#################
# OHC algorithm #
#################

for i in range(nb_repetitions):

    results = dict()

    # Generate the data
    df_model = pd.DataFrame(np.random.randint(low, high, size=(voc_size, data_size)))
    print(str(voc_size) + " vectors of " + str(data_size) + " dimensions generated")
    results["voc_size"] = voc_size

    # Compute the distance matrix
    distance_matrix = pairwise_distances(df_model, metric=met)
    # From the distance matrix to a sorted list of tuples
    li = utils.matrix_to_tuples(distance_matrix, triangle=True)
    sorted_tuples = sorted(li, key=utils.tuple_distance)
    # Normalize the distances
    min_dist = 0
    norm_factor = utils.tuple_distance(sorted_tuples[-1]) - min_dist
    sorted_tuples = [(a, b, float(d - min_dist) / norm_factor) for (a, b, d) in sorted_tuples]

    # Compute the known algorithm hierarchies as reference
    time_start = time.process_time()
    slink_matrix = linkage(df_model.values, method='single', metric=met)  # For Slink the complexity is in O(n^2)
    time_slink = time.process_time() - time_start
    # results["time_slink"] = time_slink
    print("slink executed in " + str(time_slink) + "s")
    time_start = time.process_time()
    average_matrix = linkage(df_model.values, method='average', metric=met)  # For Slink the complexity is in O(n^2)
    time_average = time.process_time() - time_start
    # results["time_average"] = time_average
    print("average executed in " + str(time_average) + "s")
    time_start = time.process_time()
    ward_matrix = linkage(df_model.values, method='ward', metric=met)  # Complexity in O(n^2) time and space
    time_ward = time.process_time() - time_start
    # results["time_ward"] = time_ward
    print("ward executed in " + str(time_ward) + "s")
    time_start = time.process_time()
    clusterer = hdbscan.HDBSCAN(min_cluster_size=2, metric='precomputed')
    clusterer.fit(distance_matrix)
    hdbscan_matrix = clusterer.single_linkage_tree_.to_numpy()
    time_hdbscan = time.process_time() - time_start
    # results["time_hdbscan"] = time_hdbscan
    print("hdbscan executed in " + str(time_hdbscan) + "s")

    # Constant similarities

    sim_ward_slink = metric.tree_similarity(ward_matrix, slink_matrix)
    results["sim_ward_slink"] = sim_ward_slink
    print("sim_ward_slink = " + str(sim_ward_slink))
    sim_ward_average = metric.tree_similarity(ward_matrix, average_matrix)
    results["sim_ward_average"] = sim_ward_average
    print("sim_ward_average = " + str(sim_ward_average))
    sim_average_slink = metric.tree_similarity(average_matrix, slink_matrix)
    results["sim_average_slink"] = sim_average_slink
    print("sim_average_slink = " + str(sim_average_slink))

    sim_hdbscan_slink = metric.tree_similarity(hdbscan_matrix, slink_matrix)
    results["sim_hdbscan_slink"] = sim_hdbscan_slink
    print("sim_hdbscan_slink = " + str(sim_hdbscan_slink))

    # Generate quasi-dendrograms and compute the corresponding similarities for different merging criteria

    name = 'random'
    l = l_max
    while l >= 0:
        print("iteration: " + str(i+1) + "/" + str(nb_repetitions) + ", merging criterion: " + str(l))
        logs = dict()
        # Run the algo for a given merging criterion
        ohc_hierarchy = clustering.hierarchical_clustering_with_overlapping(sorted_tuples, merging_criterion=l,
                                                                            batch_size=batch_size, normalize=True,
                                                                            name=name, labels=df_model.index,
                                                                            time_limit=time_limit, logs=logs)
        if logs["time"] == -1:
            logs["time"] = "inf"
            print("OHC time limit exceeded")
            break

        # Compute the similarities
        results["sim_ohc_slink_" + str(l)] = metric.ohc_tree_similarity(ohc_hierarchy, slink_matrix)
        print("sim_ohc_slink = " + str(results["sim_ohc_slink_" + str(l)]))

        results["sim_ohc_ward_" + str(l)] = metric.ohc_tree_similarity(ohc_hierarchy, ward_matrix)
        print("sim_ohc_ward = " + str(results["sim_ohc_ward_" + str(l)]))

        results["sim_ohc_average_" + str(l)] = metric.ohc_tree_similarity(ohc_hierarchy, ward_matrix)
        print("sim_ohc_average = " + str(results["sim_ohc_average_" + str(l)]))

        results["sim_ohc_hdbscan_" + str(l)] = metric.ohc_tree_similarity(ohc_hierarchy, hdbscan_matrix)
        print("sim_ohc_hdbscan = " + str(results["sim_ohc_hdbscan_" + str(l)]))

        if l == 0:
            break
        else:
            if l/2 <= l_min:
                l = 0
            else:
                l = l/2

    # If the file doesn't exist we need to add the csv header
    if not os.path.isfile(log_name):
        with open(log_name, "w+") as log_file:
            header = ';'.join(list(results.keys())) + "\n"
            log_file.write(header)
    # Write the line in the file
    with open(log_name, "a+") as log_file:
        log_file.write(';'.join([str(v) for v in list(results.values())]).replace('.', ',') + "\n")

print('Logs saved in ' + log_name)

