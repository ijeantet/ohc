## OHC

This repository contains the source code of the Overlapping Hierarchical Clustering algorithm developed by the druid team at IRISA Rennes.

A tutorial is available with the Jupyter Notebook _[OHC_tutorial.ipynb](https://gitlab.inria.fr/ijeantet/ohc/-/blob/master/OHC_tutorial.ipynb)_.

Details can be found in the following paper in open access:

Jeantet, I., Miklós, Z., & Gross-Amblard, D. (2020, April). **Overlapping Hierarchical Clustering (OHC)**. In _International Symposium on Intelligent Data Analysis_ (pp. 261-273). Springer, Cham. https://doi.org/10.1007/978-3-030-44584-3_21

## Dependencies

This code was developed in a Python 3 environment. Some dependencies are specific to word embedding and may not be mandatory depending on your use case.

* ast
* bs4
* gensim
* graphviz
* hdbscan
* ipywidgets
* jupyterlab
* math
* networkx
* nltk
* numpy
* pandas
* pickle
* scipy
* sklearn
* sortedcontainers
* unidecode

## Usage
(_./lib_)

from _clustering.py_:
* hierarchical_clustering_with_overlapping
* preprocess_hierarchy

from _visualisation.py_
* hierarchy_to_graph

from _metric.py_:
* tree_similarity
* ohc_tree_similarity
* ohc_similarity

## Experiments
(_./experiments_)

The raw data used to generate some figures of the paper can be found in this the _results_ subfolder.

* To reproduce results from Figure 5a, run _test_execution_time.py_.
* To reproduce results from Figure 5b, run _test_similarity.py_.

Unfortunately these scripts are not configurable in command line, you have to dig into it if you want to change something (the number of vectors for instance).

## Author

* **Ian Jeantet** - *PhD student* - <ian.jeantet@irisa.fr>

## License

Copyright (C) 2018-2021 IRISA - Univ. Rennes 1

_This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version._

_This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details._

_You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>._

## Acknowledgments

* The ANR Program : [ANR-16-CE38-0002](http://www.agence-nationale-recherche.fr/Projet-ANR-16-CE38-0002)
* Our partners on the project:
  - [IHPST](http://ihpst.cnrs.fr/) - Institut d'Histoire et de Philosophie des Sciences et Techniques
  - [ISC-PIF](https://iscpif.fr/) -  Institut des Systèmes Complexes de Paris Ile de France
  - [LIP6](https://www.lip6.fr/) - Laboratoire Informatique de Paris 6
* Hat tip to anyone whose code was used
