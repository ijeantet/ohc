{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Build, analyse and compare quasi-dendrograms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Library importations\n",
    "# Build-in\n",
    "import logging\n",
    "import sys\n",
    "import multiprocessing\n",
    "import os\n",
    "import time\n",
    "# External\n",
    "import pandas as pd\n",
    "import graphviz\n",
    "import networkx as nx\n",
    "import matplotlib.pyplot as plt # To draw a networkx graph\n",
    "import gensim\n",
    "from sklearn.metrics.pairwise import pairwise_distances\n",
    "import ipywidgets as widgets\n",
    "from IPython.display import display\n",
    "import numpy as np\n",
    "import pickle\n",
    "import unidecode\n",
    "import random\n",
    "import plotly.graph_objects as go\n",
    "# Local files\n",
    "import lib.utils as utils\n",
    "import lib.data as data\n",
    "import lib.embedding as embedding\n",
    "import lib.clustering as clustering\n",
    "import lib.visualisation as visu\n",
    "import lib.metric as metric\n",
    "\n",
    "# General configuration\n",
    "# Pass logging.INFO as level to display INFO events\n",
    "logger = utils.log_config(level=logging.WARNING)\n",
    " \n",
    "print(\"Configuration done\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data management\n",
    "\n",
    "- Extract specific data from source files\n",
    "\n",
    "In this example the data are extracted from Json files but it can work for xml or csv files (see read_xml_file or read_csv_file). Titles + abstracts are grouped by years and stored in several files where one line equals one entry."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "file_name = \"./data/wiley/wiley_json/\"\n",
    "saving_path = \"./data/wiley/raw_data/\"\n",
    "# record_names = ['article', 'inproceedings', 'proceedings', 'book', 'incollection', 'phdthesis', 'mastersthesis', 'data']\n",
    "tag_names = ['title', 'abstract']\n",
    "sort_by = 'date'\n",
    "output_prefix = 'wiley'\n",
    "\n",
    "data.read_json_file(file_name, tag_names=tag_names, output_prefix=output_prefix, saving_path=saving_path, sort_by=sort_by)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Clean the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_path = \"./data/wiley/raw_data/\"\n",
    "saving_path = \"./data/wiley/clean_data/\"\n",
    "contracted_words = \"./data/contracted_words.pickle\"\n",
    "lemmatize = True\n",
    "\n",
    "with open(contracted_words, 'rb') as file:\n",
    "     d_contr_words = pickle.load(file)        \n",
    "data.clean_files(data_path, saving_path=saving_path, token_min_len=2, token_max_len=50, lemmatize=lemmatize, contracted_words=d_contr_words)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Sub-sample the data (if necessary)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_path = \"./data/wiley/clean_data/\"\n",
    "saving_path = \"./data/wiley/clean_data_sub_50/\"\n",
    "p = 0.5\n",
    "data.sub_sampling(data_path, saving_path=saving_path, p=p)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Transform the data into n-grams"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "data_path = \"./data/wiley/clean_data/\"\n",
    "ngrams_path = \"./data/wiley/ngrams/\"\n",
    "stop_word_file = \"./data/stop_words_english_gensim_nltk_scikit_learn_390.txt\"\n",
    "max_length = 3\n",
    "\n",
    "stoplist = set()\n",
    "with open(stop_word_file) as stop_words:\n",
    "    stoplist = set(stop_words.read().split('\\n'))\n",
    "\n",
    "data.ngram_transformer(data_path, ngrams_path, ngram_length=max_length, min_count=5, threshold=10, common_terms=stoplist)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model generation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Generate the models from the data where one line = one entry with terms separated by whitespace.\n",
    "                    \n",
    "Parameters:         \n",
    "- data_path:        Path of the data files\n",
    "- period:           Type of period that formatted the data.\n",
    "- period_size:      Number of periods to concatenate.\n",
    "- step:             Number of periods between consecutive models. if step < period_size there will be an overlap.\n",
    "- max_final_vocab:  Maximal number of terms per model based on their occurrence in the data. -1 for no limitation.\n",
    "- min_count:        The minimal number of occurences of a term to be part of the vocabulary.\n",
    "- opt:              Option to determine the way the vocabulary is selected. Value in {1:previous, 2:union, 3:intersection, 4:new, 5:fixed}.\n",
    "- stop_word_file:   List of stop words to ignore during the training.\n",
    "- models_path:      Path where to save the models."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "data_path = \"./data/wiley/ngrams/\"\n",
    "period = \"date\"\n",
    "period_size = 3\n",
    "step=1\n",
    "max_final_vocab = 500 # -1 for infinite\n",
    "min_count = 10\n",
    "opt = 4 # vocabulary option in {1:previous, 2:union, 3:intersection, 4:new, 5:fixed}\n",
    "stop_word_file = \"./data/stop_words_english_gensim_nltk_scikit_learn_390.txt\"\n",
    "models_path = \"./models/wiley/win_\" + str(period_size) + \"/step_\" + str(step) + \"/vocab_\" + str(max_final_vocab) # Generated from the parameters\n",
    "\n",
    "# Embedding options: see gensim documentation for details\n",
    "embedding_size = 300\n",
    "learning_rate = 0.025\n",
    "window_size = 2\n",
    "subsample = 1e-3\n",
    "workers = multiprocessing.cpu_count()\n",
    "sg = True\n",
    "num_samples = 5\n",
    "epochs_to_train = 5\n",
    "batch_size = 100\n",
    "\n",
    "embedding.generate_embeddings(data_path, models_path, opt=opt, period_size=period_size, period=period, step=step, stop_word_file=stop_word_file, embedding_size=embedding_size, min_count=min_count, max_final_vocab=max_final_vocab)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hierarchy generation\n",
    "\n",
    "### Models to list of distances\n",
    "\n",
    "Transform the models to a list of increasing pairwise distances\n",
    "\n",
    "Parameters:\n",
    "- data_path:        Path of the data files.\n",
    "- pair_dist_path:   Path where to store the pairwise distance.\n",
    "- normalize:        Boolean wheter to normalize the distances or not.\n",
    "- met:              Metric to use for the distances. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_path = \"./models/wiley/win_3/step_1/vocab_1000/\"\n",
    "pair_dist_path = \"./models/wiley/lem/win_3/step_1/vocab_1000/\"\n",
    "normalize = True\n",
    "met = 'euclidean'\n",
    "\n",
    "# Files path management (if data_path is a single file or a folder)\n",
    "dir_path, files = utils.data_crawl(data_path)\n",
    "\n",
    "print(\"in \" + dir_path)\n",
    "nb_models = 0\n",
    "for file_name in sorted(files):\n",
    "    if file_name.endswith(\".word2vec.csv\"):\n",
    "        nb_models += 1\n",
    "        logger.info(\"Processing \" + file_name)\n",
    "\n",
    "        # Load the model\n",
    "        df_model = embedding.load_model_from_csv(os.path.join(dir_path, file_name), sort=False)\n",
    "        df_freqs = df_model[\"Frequency\"]\n",
    "        df_labels = df_model.index\n",
    "        df_model = df_model.iloc[:, 0:-1]  # Remove the frequency column\n",
    "        name= '.'.join(file_name.split('/')[-1].split('.')[:-2])\n",
    "        print(file_name.split('/')[-1] + \" loaded -> \" + str(df_model.shape[0]) + \" word vectors\")\n",
    "\n",
    "        # Compute the sorted pairwise distance list\n",
    "        sorted_li = utils.vectors_to_tuples(df_model, metric=met)\n",
    "        if normalize and sorted_li:\n",
    "            min_dist = 0\n",
    "            norm_factor = utils.tuple_distance(sorted_li[-1]) - min_dist\n",
    "            sorted_li = [(a, b, float(d-min_dist)/norm_factor) for (a, b, d) in sorted_li]\n",
    "        \n",
    "        # Save the distance list\n",
    "        utils.save_to_pickle(pair_dist_path, sorted_li, name=name+\".metric_\"+met+\".distances\")\n",
    "\n",
    "        if not nb_models % 10: \n",
    "                print(str(nb_models) + \" models processed\")\n",
    "print(str(nb_models) + \" models processed\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Build hierarchies over the models\n",
    "\n",
    "- From scratch (optimize the distances computation by loading/saving them from/in a file)\n",
    "\n",
    "    Parameters:\n",
    "    - data_path:        Path of the data files.\n",
    "    - hierarchies_path: Path where to store the pairwise distance.\n",
    "    - normalize:        Boolean wheter to normalize the distances or not.\n",
    "    - met:              Metric to use for the distances. \n",
    "    - l:                Merging criterion used for update the clusters.\n",
    "    - batch_size:       Number of sets of distances to add between each cluster update (from delta_i to delta_(i+batch_size))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_path = \"./models/wiley/win_3/step_1/vocab_1000/\"\n",
    "hierarchies_path = \"./hierarchies/wiley/win_3/step_1/vocab_1000/\"\n",
    "normalize=True\n",
    "met = 'cosine'\n",
    "l = 0.1 # Merging criterion\n",
    "batch_size = 1\n",
    "\n",
    "# Files path management (if data_path is a single file or a folder)\n",
    "dir_path, files = utils.data_crawl(data_path)\n",
    "model_files = sorted([f for f in files if f.endswith(\".word2vec.csv\")])\n",
    "print(str(len(model_files)) + \" models found in \" + dir_path)\n",
    "\n",
    "#Init widgets\n",
    "wid_global = widgets.VBox()\n",
    "wid_tab_content = []\n",
    "wid_tab = widgets.Tab(children=wid_tab_content)\n",
    "wid_nb_mobels = widgets.Button(description=\"0\")\n",
    "wid_models = widgets.HBox([widgets.Button(description=\"Number of models\"), wid_nb_mobels])\n",
    "wid_global.children = [wid_tab, wid_models]\n",
    "display(wid_global)\n",
    "\n",
    "nb_models = 0\n",
    "for file_name in model_files:\n",
    "    nb_models += 1\n",
    "    wid_nb_mobels.description = str(nb_models)\n",
    "    logger.info(\"Processing \" + file_name)\n",
    "\n",
    "    # Create a new widget for the current file\n",
    "    wid_file_content = []\n",
    "    wid_file = widgets.VBox(children=wid_file_content)\n",
    "    wid_tab_content.append(wid_file)\n",
    "    wid_tab.children = wid_tab_content\n",
    "    wid_tab.set_title(nb_models-1, str(nb_models))\n",
    "\n",
    "\n",
    "    # Load the model\n",
    "    df_model = embedding.load_model_from_csv(os.path.join(dir_path, file_name), sort=False)\n",
    "    df_freqs = df_model[\"Frequency\"]\n",
    "    df_labels = df_model.index\n",
    "    df_model = df_model.iloc[:, 0:-1]  # Remove the frequency column\n",
    "    name= '.'.join(file_name.split('.')[:-2])\n",
    "    # Update current widget\n",
    "    wid_file_content.append(widgets.HTML(value=file_name + \" loaded -> \" + str(df_model.shape[0]) + \" word vectors\"))\n",
    "    wid_file.children = wid_file_content\n",
    "\n",
    "\n",
    "    # Look if an associated distance file exists to avoid to recompute them.\n",
    "    distance_path = os.path.join(dir_path, name + \".metric_\" + met + \".distances.pickle\")\n",
    "    if os.path.isfile(distance_path):\n",
    "        # Load the distance file\n",
    "        sorted_tuples = utils.load_from_pickle(distance_path)\n",
    "        # Update current widget tab\n",
    "        wid_file_content.append(widgets.HTML(value=distance_path.split('/')[-1] + \" loaded -> \" + str(len(sorted_tuples)) + \" tuples\"))\n",
    "        wid_file.children = wid_file_content\n",
    "    else:\n",
    "        sorted_tuples = utils.vectors_to_tuples(df_model, metric=met, wid_container=wid_file)\n",
    "        if normalize and sorted_tuples:\n",
    "            min_dist = 0\n",
    "            norm_factor = utils.tuple_distance(sorted_tuples[-1]) - min_dist\n",
    "            sorted_tuples = [(a, b, float(d-min_dist)/norm_factor) for (a, b, d) in sorted_tuples]\n",
    "        # Save the distance list for a later use\n",
    "        utils.save_to_pickle(dir_path, sorted_tuples, name=name+\".metric_\"+met+\".distances\", wid_container=wid_file)\n",
    "\n",
    "    # Compute the hierarchy\n",
    "    ohc_hierarchy = clustering.hierarchical_clustering_with_overlapping(sorted_tuples, merging_criterion=l, batch_size=batch_size, normalize=normalize, name=name, labels=df_labels, freqs=df_freqs, wid_container=wid_file)\n",
    "\n",
    "    # Save the hieararchy\n",
    "    utils.save_to_pickle(hierarchies_path, ohc_hierarchy, wid_container=wid_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- With a preprocessed hierarchy\n",
    "\n",
    "    Parameters:\n",
    "    - data_path:        Path of the data files.\n",
    "    - hierarchies_path: Path where to store the pairwise distance.\n",
    "    - normalize:        Boolean wheter to normalize the distances or not.\n",
    "    - met:              Metric to use for the distances. \n",
    "    - d_th:             Threshold of the distances to consider for the preprocessing\n",
    "    - l:                Merging criterion used for update the clusters.\n",
    "    - batch_size:       Number of sets of distances to add between each cluster update (from delta_i to delta_(i+batch_size))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_path = \"./models/wiley/win_3/step_1/vocab_1000/\"\n",
    "hierarchies_path = \"./hierarchies/wiley/win_3/step_1/vocab_1000/\"\n",
    "normalize=True\n",
    "met = 'cosine'\n",
    "d_th = 0.9 # Threshold of the distances to consider for the preprocessing\n",
    "l = 0.1 # Merging criterion\n",
    "batch_size = 1\n",
    "\n",
    "# Files path management (if data_path is a single file or a folder)\n",
    "dir_path, files = utils.data_crawl(data_path)\n",
    "model_files = sorted([f for f in files if f.endswith(\".word2vec.csv\")])\n",
    "print(str(len(model_files)) + \" models found in \" + dir_path)\n",
    "\n",
    "#Init widgets\n",
    "wid_global = widgets.VBox()\n",
    "wid_tab_content = []\n",
    "wid_tab = widgets.Tab(children=wid_tab_content)\n",
    "wid_nb_mobels = widgets.Button(description=\"0\")\n",
    "wid_models = widgets.HBox([widgets.Button(description=\"Number of models\"), wid_nb_mobels])\n",
    "wid_global.children = [wid_tab, wid_models]\n",
    "display(wid_global)\n",
    "\n",
    "nb_models = 0\n",
    "for file_name in model_files:\n",
    "    nb_models += 1\n",
    "    wid_nb_mobels.description = str(nb_models)\n",
    "    logger.info(\"Processing \" + file_name)\n",
    "\n",
    "    # Create a new widget for the current file\n",
    "    wid_file_content = []\n",
    "    wid_file = widgets.VBox(children=wid_file_content)\n",
    "    wid_tab_content.append(wid_file)\n",
    "    wid_tab.children = wid_tab_content\n",
    "    wid_tab.set_title(nb_models-1, str(nb_models))\n",
    "\n",
    "\n",
    "    # Load the model\n",
    "    df_model = embedding.load_model_from_csv(os.path.join(dir_path, file_name), sort=False)\n",
    "    df_freqs = df_model[\"Frequency\"]\n",
    "    df_labels = df_model.index\n",
    "    df_model = df_model.iloc[:, 0:-1]  # Remove the frequency column\n",
    "    name= '.'.join(file_name.split('.')[:-2])\n",
    "    # Update current widget\n",
    "    wid_file_content.append(widgets.HTML(value=file_name + \" loaded -> \" + str(df_model.shape[0]) + \" word vectors\"))\n",
    "    wid_file.children = wid_file_content\n",
    "\n",
    "\n",
    "    # Look if an associated distance file exists to avoid to recompute them.\n",
    "    distance_path = os.path.join(dir_path, name + \".metric_\" + met + \".distances.pickle\")\n",
    "    if os.path.isfile(distance_path):\n",
    "        # Load the distance file\n",
    "        sorted_tuples = utils.load_from_pickle(distance_path)\n",
    "        # Update current widget tab\n",
    "        wid_file_content.append(widgets.HTML(value=distance_path.split('/')[-1] + \" loaded -> \" + str(len(sorted_tuples)) + \" tuples\"))\n",
    "        wid_file.children = wid_file_content\n",
    "    else:\n",
    "        sorted_tuples = utils.vectors_to_tuples(df_model, metric=met, wid_container=wid_file)\n",
    "        if normalize and sorted_tuples:\n",
    "            min_dist = 0\n",
    "            norm_factor = utils.tuple_distance(sorted_tuples[-1]) - min_dist\n",
    "            sorted_tuples = [(a, b, float(d-min_dist)/norm_factor) for (a, b, d) in sorted_tuples]\n",
    "        # Save the distance list for a later use\n",
    "        utils.save_to_pickle(dir_path, sorted_tuples, name=name+\".metric_\"+met+\".distances\", wid_container=wid_file)\n",
    "\n",
    "    # Compute the hierarchy\n",
    "    preprocessed_hierarchy = clustering.preprocess_hierarchy(sorted_tuples, d_th, normalize=normalize, name=name, labels=df_labels, freqs=df_freqs, wid_container=wid_file)\n",
    "    ohc_hierarchy = clustering.hierarchical_clustering_with_overlapping(preprocessed_hierarchy[\"data\"], merging_criterion=l, batch_size=batch_size, preprocessed_hierarchy=preprocessed_hierarchy, wid_container=wid_file)\n",
    "\n",
    "    # Save the hieararchy\n",
    "    utils.save_to_pickle(hierarchies_path, ohc_hierarchy, wid_container=wid_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hierarchy visualisation\n",
    "\n",
    "### hierarchy to dot graph:\n",
    "                    \n",
    "Parameters:\n",
    "- hierarchy_file:   Path to the hierarchy file.\n",
    "- save:             Path where to save the graph.\n",
    "- most_freq:        Number of terms to display per clusters. The most frequent ones will be selected.\n",
    "- start:            Bottom level of the hierarchy to display. If -1 we start from the top of the hierarchy. \n",
    "- nb_levels:        Number of levels to display from start (or the top of the hierarchy if start=-1)\n",
    "- distance_axe:     If true, display an axe of distance and the right of the graph\n",
    "- density_color:    If true, color the nodes according to the evolution of their density. Else, keep them white\n",
    "- graph_format:     Format of the graphviz output. 'svg' by default"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hierarchy_file = \"./hierarchies/wiley/win_3/step_1/vocab_1000/wiley.date_1996_1998.ngrams_3.merge_0_01.batch_1.ohc.pickle\"\n",
    "save = \"./graphs/wiley/win_3/step_1/vocab_1000/\"\n",
    "most_freq = 12\n",
    "start = 0\n",
    "nb_levels = 20\n",
    "distance_axe = True\n",
    "density_color = False\n",
    "graph_format = \"svg\"\n",
    "\n",
    "ohc_hierarchy = utils.load_from_pickle(hierarchy_file)\n",
    "hg = visu.hierarchy_to_graph(ohc_hierarchy, start=start, nb_levels=nb_levels, distance_axe=distance_axe, density_color=density_color, most_freq=most_freq, save=save, graph_format=graph_format)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hierarchy comparison"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### From scratch\n",
    "\n",
    "1) Load the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Data\n",
    "model_file = \"./models/wiley/win_3/step_1/vocab_1000/wiley.date_1996_1998.ngrams_3.word2vec.csv\"\n",
    "\n",
    "df_model = embedding.load_model_from_csv(model_file, sort=False)\n",
    "df_model = df_model.iloc[:, 0:-1]  # Remove the frequency column\n",
    "df_labels = df_model.index\n",
    "name= '.'.join(model_file.split('/')[-1].split('.')[:-2])\n",
    "print(model_file.split('/')[-1] + \" loaded -> \" + str(df_model.shape[0]) + \" word vectors\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1) Or use a fake one"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = (2, 4)\n",
    "B = (2, 2)\n",
    "C = (3, 3)\n",
    "D = (4, 3)\n",
    "E = (4, 1)\n",
    "F = (6, 6)\n",
    "M = (6, 7)\n",
    "N = (7, 6)\n",
    "G = (8, 11)\n",
    "H = (10, 11)\n",
    "I = (9, 10)\n",
    "J = (10, 8)\n",
    "K = (11, 9)\n",
    "L = (2, 15)\n",
    "\n",
    "M = [A, B, C, D, E, F, G, H, I, J, K, L, M, N]\n",
    "# M = [A, C, D, E, F, H, I, J, K, L]\n",
    "df_model = pd.DataFrame(M)\n",
    "df_labels = [\"A\", 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']\n",
    "# df_freqs = [2,3,4,2,1,1,1,2,6,5,3,9]\n",
    "# df_labels = [\"A\", 'C', 'D', 'E', 'F', 'H', 'I', 'J', 'K', 'L']\n",
    "name=\"hand_made_14\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2) Perform different clustering methods (require specific publicly available packages)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from scipy.cluster.hierarchy import dendrogram, linkage\n",
    "import hdbscan\n",
    "\n",
    "# Hierarchies\n",
    "normalize = True\n",
    "l = 1\n",
    "batch_size = 1\n",
    "met = 'cosine'\n",
    "\n",
    "# Compute of load the distance matrix if existing\n",
    "distance_matrix = pairwise_distances(df_model, metric=met)\n",
    "\n",
    "# Compute the hierarchy\n",
    "ohc_hierarchy = clustering.hierarchical_clustering_with_overlapping(distance_matrix, merging_criterion=l, batch_size=batch_size, normalize=normalize, name=name, labels=df_labels)\n",
    "\n",
    "slink_matrix = linkage(df_model.values, method='single', metric=met)  # For Slink the complexity is in O(n^2)\n",
    "\n",
    "clusterer = hdbscan.HDBSCAN(min_cluster_size=2, metric='precomputed')\n",
    "clusterer.fit(distance_matrix)\n",
    "hdbscan_matrix = clusterer.single_linkage_tree_.to_numpy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display SLINK dendrogram"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "\n",
    "fig = plt.figure(figsize=(10,5))\n",
    "dn = dendrogram(slink_matrix, link_color_func=lambda k: 'black', labels=df_labels)\n",
    "plt.xlabel(\"nodes\")\n",
    "plt.ylabel(\"distance\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3) Compute the similarities between these methods"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Similatiry comparison\n",
    "\n",
    "time_start = time.process_time()\n",
    "sim_ohc_slink = metric.ohc_tree_similarity(ohc_hierarchy, slink_matrix)\n",
    "print(\"sim_ohc_slink = \" + str(sim_ohc_slink) + \" in \" + str(time.process_time() - time_start) + \"s\")\n",
    "\n",
    "time_start = time.process_time()\n",
    "sim_hdbscan_slink = metric.tree_similarity(hdbscan_matrix, slink_matrix)\n",
    "print(\"sim_hdbscan_slink = \" + str(sim_hdbscan_slink) + \" in \" + str(time.process_time() - time_start) + \"s\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### From precomputed hierarchies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hierarchy_file1 = \"./hierarchies/wiley/win_3/step_1/vocab_100/wiley.date_2001_2003.ngrams_3.merge_0_1083470594338837.batch_1.ohc.pickle\"\n",
    "hierarchy_file2 = \"./hierarchies/wiley/win_3/step_1/vocab_100/wiley.date_2003_2005.ngrams_3.merge_0_1083470594338837.batch_1.ohc.pickle\"\n",
    "\n",
    "h_source = utils.load_from_pickle(hierarchy_file1)\n",
    "h_target = utils.load_from_pickle(hierarchy_file2)\n",
    "\n",
    "time_start = time.process_time()\n",
    "sim_ohc1_ohc2 = metric.ohc_similarity(h_source, h_target)\n",
    "print(\"sim_ohc1_ohc2 = \" + str(sim_ohc1_ohc2) + \" in \" + str(time.process_time() - time_start) + \"s\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Detailled similarity study"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Compute the complete pairwise level similarity matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hierarchy_file1 = \"./hierarchies/wiley/lem/sub_100/win_3/step_1/vocab_100/wiley.date_2001_2003.ngrams_3.merge_0_1083470594338837.batch_1.ohc.pickle\"\n",
    "hierarchy_file2 = \"./hierarchies/wiley/lem/sub_100/win_3/step_1/vocab_100/wiley.date_2003_2005.ngrams_3.merge_0_1083470594338837.batch_1.ohc.pickle\"\n",
    "\n",
    "h_source = utils.load_from_pickle(hierarchy_file1)\n",
    "h_target = utils.load_from_pickle(hierarchy_file2)\n",
    "\n",
    "mat_sim_avg = [[0 for x in range(len(h_target))] for y in range(len(h_source))]\n",
    "\n",
    "#Init widgets\n",
    "wid_global = widgets.VBox()\n",
    "display(wid_global)\n",
    "\n",
    "size = len(h_source)\n",
    "if size <= 200:\n",
    "    every = 1\n",
    "else:\n",
    "    every = int(size/200)     # every 0.5%\n",
    "\n",
    "for i, l1 in utils.log_progress(enumerate(h_source.levels()), every=every, size=size, name=\"Processed levels\", wid_container=wid_global):\n",
    "    \n",
    "    clusters1 = [set(h_source.labels(c1)) for c1 in l1.clusters()]\n",
    "        \n",
    "    for j, l2 in enumerate(h_target.levels()):\n",
    "        \n",
    "        clusters2 = [set(h_target.labels(c2)) for c2 in l2.clusters()]\n",
    "        mat_sim_avg[i][j] = metric.level_similarity(clusters1, clusters2, alignment=None, strict=False, method='weighted')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display it as a Heatmap"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = go.Figure(data=go.Heatmap(\n",
    "                   z=mat_sim_avg,\n",
    "                   hoverongaps = False))\n",
    "fig.update_layout(\n",
    "#     title=\"Similarity heatmap of corresponding levels between ohc1 and ohc2\",\n",
    "    xaxis_title='level of the first quasi-dendrogram',\n",
    "    yaxis_title='level of the second quasi-dendrogram'\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Compute the detailled similarity and show the path taken in a Heatmap"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hierarchy_file1 = \"./hierarchies/wiley/lem/sub_100/win_3/step_1/vocab_100/wiley.date_2001_2003.ngrams_3.merge_0_1083470594338837.batch_1.ohc.pickle\"\n",
    "hierarchy_file2 = \"./hierarchies/wiley/lem/sub_100/win_3/step_1/vocab_100/wiley.date_2003_2005.ngrams_3.merge_0_1083470594338837.batch_1.ohc.pickle\"\n",
    "\n",
    "# Load the hierarchies\n",
    "ohc1  = utils.load_from_pickle(hierarchy_file1)\n",
    "ohc2  = utils.load_from_pickle(hierarchy_file2)\n",
    "\n",
    "# Compute the similarity\n",
    "time_start = time.process_time()\n",
    "sim_ohc = metric.ohc_similarity(ohc1, ohc2, details=True, method='avg')\n",
    "print(\"sim_ohc = \" + str(np.mean(sim_ohc[2])) + \" in \" + str(time.process_time() - time_start) + \"s\")\n",
    "\n",
    "# Plot the similarity heatmap between ohc1 and ohc2\n",
    "fig = go.Figure(data=go.Heatmap(\n",
    "                   z=sim_ohc[2],\n",
    "                   x=sim_ohc[0],\n",
    "                   y=sim_ohc[1],\n",
    "                   hoverongaps = False))\n",
    "fig.update_layout(\n",
    "#     title=\"Similarity heatmap of corresponding levels between ohc1 and ohc2\",\n",
    "    xaxis_title='level of the first quasi-dendrogram',\n",
    "    yaxis_title='level of the second quasi-dendrogram'\n",
    ")\n",
    "fig.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
