"""
    data.py:

    Copyright (C) 2018-2020 Ian Jeantet <ian.jeantet@irisa.fr>
    Copyright (C) 2018 Diego Cardenas Cabeza

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import os
import re
import json
from tempfile import mkstemp
from shutil import move
# External
from bs4 import BeautifulSoup
from nltk.stem import WordNetLemmatizer
import nltk
import gensim
import unidecode
import random

# General configuration
logger = logging.getLogger()


def extract_tags(markup_string, tag_names):
    """
    Extract tags from an markup string and return them as a dictionary.
    -----
    :param markup_string:   A markup string.
    :param tag_names:       The list of tags to extract.
    :return:                A dictionary where the keys are the tag names and the values are lists of extracted values
                            according to the key tag
    """
    dict_tags = dict()
    soup = BeautifulSoup(markup_string, "lxml")
    for tag in tag_names:
        dict_tags[tag] = [str(t.string).replace('\n', " ").replace('\r', '') for t in soup.find_all(tag)]

    return dict_tags


def read_xml_file(files_path, record_names, tag_names, saving_path=None, sort_by=None):
    """
    Read xml files line by line looking for specific tags in specific records.
    -----
    For instance: we want to extract tag_names=['title'] from record_names=['article','proceedings'] by sorting them by
                  sort_by='year'. It means that the xml files contain records named 'article' and 'proceedings' and that
                  these records contain tags named 'title' and 'year'. The extracted titles will be split in different
                  files according to the value of the tag 'year'.
    :param files_path:      Name of the file or path of the files to process.
    :param record_names:    List of records to look for in the files.
    :param tag_names:       List of tags to extract from the records.
    :param saving_path:     Optional, path where to write the processed files. If None the processed files will be
                            written in the current files_path folder.
    :param sort_by:         Optional, the extracted tags will be split in different files according to this value.
    """

    # Files path management (if files_path is a single file or a folder)
    files = []
    dir_path = ''
    if os.path.isdir(files_path):
        files = os.listdir(files_path)
        dir_path = files_path
    elif os.path.isfile(files_path):
        files.append(files_path.split('/')[-1])
        dir_path = '/'.join(files_path.split('/')[:-1])

    # Saving path management (if saving_path is given or not)
    if not saving_path:
        saving_path = dir_path
    else:
        if not os.path.exists(saving_path):
            os.makedirs(saving_path)

    num_files = 0
    # Processing
    for file_name in files:
        if file_name.endswith(".xml"):
            num_files += 1
            logger.info("Processing " + file_name)
            num_record = 0
            num_ignored_record = 0
            with open(os.path.join(dir_path, file_name), 'r') as input_file:
                output_file_prefix = '.'.join(file_name.split('.')[:-1])  # + "." + '_'.join(tag_names)
                input_buffer = ''  # Will contain the raw current record
                append = None  # Flag that indicates if we found the XML tag of the record or not
                for line in input_file:
                    if not append:  # Search for the opening tag of one of the record names
                        for record in record_names:
                            if '<' + record + ' ' in line:
                                input_buffer = line
                                append = record
                    elif '</' + append + '>' in line:  # Looking for the ending tag of the current record
                        input_buffer += line
                        num_record += 1
                        # Extract the requested tags from the current record
                        dict_tags = extract_tags(input_buffer, tag_names)
                        if sort_by:
                            # Extract the tag used to sort the extracted data
                            dict_sort_tag = extract_tags(input_buffer, [sort_by])
                            sort_value = dict_sort_tag[sort_by]
                            if sort_value:  # The sorting tag was found -> We write the data in the corresponding file
                                output_file_name = output_file_prefix + "." + sort_by + "_" + \
                                                   re.sub('[\W_]+', '_', str(sort_value[0])) + ".txt"
                                with open(os.path.join(saving_path, output_file_name), 'a+') as output_file:
                                    # Concatenate the tags on one line
                                    for val in dict_tags.values():
                                        output_file.write('.'.join(val) + '\n')

                            else:  # The sorting tag was not found -> Ignore the data
                                num_ignored_record += 1

                        else:
                            with open(os.path.join(saving_path, output_file_prefix + ".txt"), 'a+') as output_file:
                                for val in dict_tags.values():
                                    output_file.write('.'.join(val) + '\n')

                        input_buffer = ''
                        append = None

                        if not num_record % 10000:
                            print(str(num_record) + " records processed (including " + str(num_ignored_record)
                                  + " ignored records)")

                    else:  # Between the tags of the record
                        input_buffer += line

                print(str(num_record) + " records processed (including " + str(num_ignored_record)
                      + " ignored records)")
            if not num_files % 10:
                print(str(num_files) + " files processed")
    print(str(num_files) + " files processed")


def read_csv_file(files_path, tag_names, sep=',', saving_path=None, sort_by=None, tag_error=True):
    """
    Read a csv file, extract some tags and optionally group them according to another tag.
    -----
    :param files_path:  Name of the file or path of the files to process.
    :param tag_names:   List of tags to extract from the records.
    :param sep:         CSV separator.
    :param saving_path: Optional, path where to write the processed files. If None the processed files will be
                        written in the current files_path folder.
    :param sort_by:     Optional, the extracted tags will be split in different files according to this value.
    :param tag_error:   Optional, if true it will raise an error if a tag cannot be found in a record,
                        i.e. if there are not enough separators.
    """

    # Files path management (if files_path is a single file or a folder)
    files = []
    dir_path = ''
    if os.path.isdir(files_path):
        files = os.listdir(files_path)
        dir_path = files_path
    elif os.path.isfile(files_path):
        files.append(files_path.split('/')[-1])
        dir_path = '/'.join(files_path.split('/')[:-1])

    # Saving path management (if saving_path is given or not)
    if not saving_path:
        saving_path = dir_path
    else:
        if not os.path.exists(saving_path):
            os.makedirs(saving_path)

    num_files = 0
    # Processing
    for file_name in files:
        if file_name.endswith(".csv"):
            num_files += 1
            logger.info("Processing " + file_name)
            with open(os.path.join(dir_path, file_name), 'r') as input_file:
                output_file_prefix = '.'.join(file_name.split('.')[:-1])

                # Process the header and get the column numbers of the tags to extract
                header = input_file.readline()[:-1].split(sep)
                tag_indices = [i for i, t in enumerate(header) if t in tag_names]
                sort_index = -1
                if sort_by:
                    for i, t in enumerate(header):
                        if t == sort_by:
                            sort_index = i
                            break

                for line in input_file:
                    # Extract the tag values
                    li = line.split(sep)
                    # Always true if tag_error is true. Check i value otherwise.
                    tab_values = [li[i] for i in tag_indices if tag_error or i < len(li)]
                    if 0 <= sort_index < len(li):
                        sort_value = li[sort_index]
                        output_file_name = output_file_prefix + "." + sort_by + "_" + re.sub('[\W_]+', '_',
                                                                                             str(sort_value)) + ".txt"
                        with open(os.path.join(saving_path, output_file_name), 'a+') as output_file:
                            # Concatenate the tags on one line
                            output_file.write('.'.join(tab_values) + '\n')
                    else:
                        with open(os.path.join(saving_path, output_file_prefix + ".txt"), 'a+') as output_file:
                            output_file.write('.'.join(tab_values) + '\n')

            if not num_files % 10:
                print(str(num_files) + " files processed")
    print(str(num_files) + " files processed")


def read_json_file(files_path, tag_names, output_prefix=None, saving_path=None, sort_by=None):
    """
    Read json files line by line looking for specific tags in all records. One line must correspond to one record.
    -----
    For instance: we want to extract tag_names=['title'] from record_names=['article','proceedings'] by sorting them by
                 sort_by='year'. It means that the json files contain records named 'article' and 'proceedings' and that
                 these records contain tags named 'title' and 'year'. The extracted titles will be split in different
                 files according to the value of the tag 'year'.
    :param files_path:      Name of the file or path of the files to process.
    :param tag_names:       List of tags to extract from the records.
    :param output_prefix:   Prefix of the output files.
    :param saving_path:     Optional, path where to write the processed files. If None the processed files will be
                            written in the current files_path folder.
    :param sort_by:         Optional, the extracted tags will be split in different files according to this value.
    """

    # Files path management (if files_path is a single file or a folder)
    files = []
    dir_path = ''
    if os.path.isdir(files_path):
        files = os.listdir(files_path)
        dir_path = files_path
    elif os.path.isfile(files_path):
        files.append(files_path.split('/')[-1])
        dir_path = '/'.join(files_path.split('/')[:-1])

    # Saving path management (if saving_path is given or not)
    if not saving_path:
        saving_path = dir_path
    else:
        if not os.path.exists(saving_path):
            os.makedirs(saving_path)

    num_files = 0
    # Processing
    for file_name in files:
        if file_name.endswith(".json"):
            num_files += 1
            logger.info("Processing " + file_name)
            num_record = 0
            num_ignored_record = 0
            with open(os.path.join(dir_path, file_name), 'r') as input_file:
                output_file_pref = output_prefix
                if not output_file_pref:
                    output_file_pref = '.'.join(file_name.split('.')[:-1])
                for line in input_file:
                    # Format the record as a Json object
                    json_record = json.loads(line)
                    num_record += 1
                    # Extract the requested tags from the current record
                    dict_tags = [json_record[tag] for tag in tag_names]
                    if sort_by:
                        try:
                            # Extract the tag used to sort the extracted data
                            sort_value = json_record[sort_by]

                            # The sorting tag was found -> We write the data in the corresponding file
                            output_file_name = output_file_pref + "." + sort_by + "_" + str(sort_value) + ".txt"
                            with open(os.path.join(saving_path, output_file_name), 'a+') as output_file:
                                # Write the concatenated tags on one line
                                output_file.write('.'.join(dict_tags) + '\n')
                        except KeyError:
                            # The sorting tag was not found -> Ignore the data
                            num_ignored_record += 1
                    else:
                        with open(os.path.join(saving_path, output_file_pref + ".txt"), 'a+') as output_file:
                            output_file.write('.'.join(dict_tags) + '\n')

                    if not num_record % 10000:
                        logger.info(str(num_record) + " records processed (including " + str(num_ignored_record)
                                    + " ignored records)")
                logger.info(str(num_record) + " records processed (including " + str(num_ignored_record)
                            + " ignored records)")
            if not num_files % 10:
                print(str(num_files) + " files processed")
    print(str(num_files) + " files processed")


# POS_tag to Wordnet_tag function
def wnpos(e):
    if e[0].lower() == 'j':
        return 'a'
    elif e[0].lower() in ['n', 'r', 'v']:
        return e[0].lower()
    else:
        return 'n'


def clean_files(files_path, saving_path=None, token_min_len=2, token_max_len=15, lemmatize=False, stopwords=None,
                contracted_words=None):
    """
    Clean the input files doing tokenization of their contents and writes new files with the tokenized words.
    -----
    :param files_path:      Name of the file or path of the files to clean.
    :param saving_path:     Optional, path where to write the cleaned files. If None the processed files will be
                            overwritten.
    :param token_min_len:   Minimum length of token (inclusive). Shorter tokens are discarded.
    :param token_max_len:   Maximum length of token in result (inclusive). Longer tokens are discarded.
    :param lemmatize:       Optional, perform a Wordnet NLTK Lemmatization (require to download 'punkt', 'wordnet',
                            'averaged_perceptron_tagger')
    :param stopwords:       Optional, set of stopwords to discard if found during the cleaning.
    :param contracted_words:Optional, dict of contracted/decontracted words to replace during the cleaning.
    """

    # Files path management (if files_path is a single file or a folder)
    if stopwords is None:
        stopwords = []
    if contracted_words is None:
        contracted_words = dict()
    lemmatizer = None
    if lemmatize:
        # Init the Wordnet Lemmatizer
        lemmatizer = WordNetLemmatizer()
    files = []
    dir_path = ''
    if os.path.isdir(files_path):
        files = os.listdir(files_path)
        dir_path = files_path
    elif os.path.isfile(files_path):
        files.append(files_path.split('/')[-1])
        dir_path = '/'.join(files_path.split('/')[:-1])

    # Saving path management (if saving_path is given or not)
    if not saving_path:
        logging.warning("During the cleaning the files are overwritten!\n "
                        "Use the 'saving_path' parameter to avoid that.")
    else:
        if not os.path.exists(saving_path):
            os.makedirs(saving_path)
        if os.path.samefile(saving_path, dir_path):
            logging.warning("During the cleaning the files are overwritten!\n "
                            "Use the 'saving_path' parameter to avoid that.")

    space = " "
    num_lines = 0
    num_files = 0
    # Processing
    for file_name in files:
        if file_name.endswith(".txt"):
            num_files += 1
            logger.info("Processing " + file_name)
            # Build the file names
            input_file_name = os.path.join(dir_path, file_name)
            # Create a temporary file in case saving_path was not specified or the same as dir_path
            if not saving_path or os.path.samefile(saving_path, dir_path):
                output_file_name = mkstemp()[1]
            else:
                output_file_name = os.path.join(saving_path, file_name)
            # Process the lines
            with open(input_file_name, 'r') as input_file, open(output_file_name, 'w') as output_file:
                i = 0
                for line in input_file:
                    # Deal with the contracted forms of words like "it's" or "shouldn't"
                    cw_line = []
                    for token in line.split(' '):
                        token = token.lower()
                        if "'" in token:
                            for cw in contracted_words.keys():
                                token = token.replace(cw, contracted_words[cw])
                        cw_line.append(token)
                    line = space.join(cw_line)
                    # Perform an nltk lemmatization of the text
                    if lemmatize:
                        # Detect sentences composing the line (that usually contains title + abstract)
                        sentences = nltk.sent_tokenize(line)
                        lem_sentences = ""
                        for sentence in sentences:
                            # Get all the pos_tag of the tokens of the sentence
                            tags = nltk.pos_tag(nltk.word_tokenize(sentence))
                            # Lemmetize the sentence with Wordnet
                            lem_sentence = " ".join([lemmatizer.lemmatize(w, wnpos(t)) for (w, t) in tags])
                            # Aggregate the sentences
                            lem_sentences += " " + lem_sentence
                        line = lem_sentences

                    # Tokenize and clean the line
                    tok_line = [token for token in gensim.utils.simple_preprocess(line, deacc=True,
                                                                                  min_len=token_min_len,
                                                                                  max_len=token_max_len)
                                if token not in stopwords]
                    output_file.write(unidecode.unidecode(space.join(tok_line)) + "\n")
                    # Transforms the vector of tokens in a unique string containing the tokens
                    # whom the characters were transform in ASCII character.
                    # The tokens are separated by the character space and the 'stopwords' are removed
                    i = i + 1
                    if not i % 10000:
                        logger.info("Saved " + str(i) + " lines")
                logger.info("Saved " + str(i) + " lines")
                num_lines += i
            # Replace the file in case saving_path was not specified or the same as dir_path
            if not saving_path or os.path.samefile(saving_path, dir_path):
                # Remove original file
                os.remove(input_file_name)
                # Move new file
                move(output_file_name, input_file_name)
            if not num_files % 10:
                print(str(num_files) + " files processed")
    print(str(num_files) + " files processed")
    print("Average of " + str(num_lines / num_files) + " lines per file")


def sub_sampling(files_path, saving_path, p=1):
    """
    Sub-sample the data keeping only a fraction of p entries in each file.
    -----
    :param files_path:  Name of the file or path of the files to sub_sample.
    :param saving_path: Path where to write the sub-sampled files. If it is the same as files_path the processed files
                        will be overwritten.
    :param p:           Fraction of entries to keep. If p=0.9, 10% of the entries will be discarded.
    """

    # Files path management (if files_path is a single file or a folder)
    files = []
    dir_path = ''
    if os.path.isdir(files_path):
        files = os.listdir(files_path)
        dir_path = files_path
    elif os.path.isfile(files_path):
        files.append(files_path.split('/')[-1])
        dir_path = '/'.join(files_path.split('/')[:-1])

    # Saving path management (if saving_path is given or not)
    if not saving_path:
        saving_path = dir_path
    else:
        if not os.path.exists(saving_path):
            os.makedirs(saving_path)

    if os.path.samefile(saving_path, dir_path):
        logging.warning("When sub-sampling the files are overwritten!")

    num_lines = 0
    num_lines_kept = 0
    num_files = 0
    # Processing
    for file_name in files:
        if file_name.endswith(".txt"):
            num_files += 1
            logger.info("Processing " + file_name)
            # Build the file names
            input_file_name = os.path.join(dir_path, file_name)
            # Create a temporary file in case saving_path was not specified or the same as dir_path
            if not saving_path or os.path.samefile(saving_path, dir_path):
                output_file_name = mkstemp()[1]
            else:
                output_file_name = os.path.join(saving_path, file_name)
            # Process the lines
            with open(input_file_name, 'r') as input_file, open(output_file_name, 'w') as output_file:
                i = 0
                j = 0
                for line in input_file:
                    # Keep the entry with a probability corresponding to the parameter p
                    if random.random() <= p:
                        output_file.write(line + "\n")
                        j = j + 1
                    i = i + 1
                    if not i % 10000:
                        logger.info(str(i) + " entries processed")
                logger.info(str(i) + " entries processed and " + str(j) + " entries saved")
                num_lines += i
                num_lines_kept += j
                # Replace the file in case saving_path was not specified or the same as dir_path
            if not saving_path or os.path.samefile(saving_path, dir_path):
                # Remove original file
                os.remove(input_file_name)
                # Move new file
                move(output_file_name, input_file_name)
            if not num_files % 10:
                print(str(num_files) + " files processed")
    print("Total of " + str(num_files) + " files processed")
    print(str(num_lines_kept) + "/" + str(num_lines) + " = " + str(num_lines_kept / num_lines) + " entries kept")
    print("Average of " + str(num_lines_kept / num_files) + " entries per file")


def ngram_transformer(files_path, saving_path=None, ngram_length=3, min_count=5, threshold=10,
                      common_terms=frozenset([])):
    """
    Look for n-grams in the data and transform them in consequence.
    -----
    :param files_path:      Name of the file or path of the files to transform into n-grams.
    :param saving_path:     Optional, path where to write the n-gram files. If None the processed files will be
                            written in the current files_path folder.
    :param ngram_length:    Maximal size of the n-grams.
    :param min_count:       Ignore all words and n-grams with total collected count lower than this value.
    :param threshold:       Represent a score threshold for forming the phrases (higher means fewer phrases).
    :param common_terms:    Optional, list of “stop words” that won’t affect frequency count of expressions containing
                            them.
    """

    # Files path management (if files_path is a single file or a folder)
    files = []
    dir_path = ''
    if os.path.isdir(files_path):
        files = os.listdir(files_path)
        dir_path = files_path
    elif os.path.isfile(files_path):
        files.append(files_path.split('/')[-1])
        dir_path = '/'.join(files_path.split('/')[:-1])

    # Saving path management (if saving_path is given or not)
    if not saving_path:
        saving_path = dir_path
    else:
        if not os.path.exists(saving_path):
            os.makedirs(saving_path)

    space = " "
    num_files = 0
    # Processing
    if ngram_length > 1:
        for file_name in files:
            if file_name.endswith(".txt"):
                num_files += 1
                logger.info("Processing " + file_name)

                # Compute the n-grams
                sentences = gensim.models.word2vec.LineSentence(
                    os.path.join(dir_path, file_name))
                phrases = gensim.models.phrases.Phrases(sentences, min_count=min_count, threshold=threshold,
                                                        common_terms=common_terms)
                ngram = gensim.models.phrases.Phraser(phrases)

                for _ in range(1, ngram_length - 1):
                    sentences = list(ngram[sentences])
                    phrases = gensim.models.phrases.Phrases(sentences, min_count=min_count, threshold=threshold,
                                                            common_terms=common_terms)
                    ngram = gensim.models.phrases.Phraser(phrases)
                sentences = list(ngram[sentences])

                # Write the n-grams in the file
                output_file_prefix = os.path.join(saving_path, '.'.join(str(file_name.split('/')[-1]).split('.')[:-1]))
                output_file_name = output_file_prefix + ".ngrams_" + str(ngram_length) + ".txt"
                with open(output_file_name, 'w') as output_file:
                    output_file.write('\n'.join([unidecode.unidecode(space.join(text)) for text in sentences]) + '\n')
                if not num_files % 10:
                    print(str(num_files) + " files processed")
        print(str(num_files) + " files processed")

        if len(files) == 1:
            return output_file_name
        else:
            return saving_path
    return dir_path
