"""
metric.py

Copyright (C) 2018-2020 Ian Jeantet <ian.jeantet@irisa.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import pandas as pd
from scipy.cluster.hierarchy import cut_tree
from ast import literal_eval


def jaccard_similarity(a, b):
    """
    Compute the Jaccard similarity between 2 sets
    -----
    The Jaccard similarity between 2 sets a and b is defined by d(a,b) = |a ∩ b| / |a ∪ b|
    """
    c = a.intersection(b)
    return float(len(c)) / (len(a) + len(b) - len(c))


def jaccard_sim_clusters(clusters1, clusters2):
    """
    Return a matrix containing the jaccard similarity between a cluster c from clusters1 and a cluster c' from clusters2
    -----
    The Jaccard similarity between 2 clusters c and c' is defined by d(c,c') = |c ∩ c'| / |c ∪ c'|

    clusters1 and clusters2 should be a list of clusters, ie a list of lists of words.
    """
    links = []
    for c1 in clusters1:
        row = []
        for c2 in clusters2:
            row.append(jaccard_similarity(set(c1), set(c2)))
        links.append(row)
    return np.array(links)


def average_clusters_similarity(clusters1, clusters2):
    df_jaccard = jaccard_sim_clusters(clusters1, clusters2)
    return df_jaccard.max(0).mean(), df_jaccard.max(1).mean()


def level_similarity(clusters1, clusters2):
    """
    Compute the similarity between 2 sets of clusters.
    -----
    :param clusters1:   List of lists of elements that form the clusters
    :param clusters2:   List of lists of elements that form the clusters
    :return:            Return a value of similarity as a float between 0 and 1
    """
    sim1, sim2 = average_clusters_similarity(clusters1, clusters2)
    return (sim1 + sim2) / 2


def ohc_tree_similarity(ohc_hierarchy, scipy_tree_matrix):
    """
    Compute the similarity between an OHC hierarchy and a dendrogram represented as a scipy tree matrix.
    -----
    :param ohc_hierarchy:       An OHC object
    :param scipy_tree_matrix:   A scipy tree matrix
    :return:                    Return a value of similarity as a float between 0 and 1
    """
    similarity = 0
    scipy_clusters = []
    df_sim = None
    levels = ohc_hierarchy.levels()
    for level in levels:
        ohc_clusters = level.clusters()
        n = len(ohc_clusters)
        # Compute scipy_clusters only if the length of ohc_clusters has changed
        if len(scipy_clusters) != n:
            scipy_cutree = cut_tree(scipy_tree_matrix, n_clusters=n)
            scipy_clusters = [[] for _ in range(len(ohc_clusters))]
            for (i, v) in enumerate(scipy_cutree):
                scipy_clusters[v[0]].append(i)
        # Initialization
        if df_sim is None:
            df_sim = pd.DataFrame(jaccard_sim_clusters(ohc_clusters, scipy_clusters),
                                  columns=[str(c) for c in scipy_clusters])
            df_sim["index"] = [str(c) for c in ohc_clusters]
            df_sim = df_sim.set_index("index")
            df_sim.index.name = None
        else:
            # Determine the changed in ohc_clusters and scipy_clusters
            ohc_removed = set(df_sim.index) - set([str(c) for c in ohc_clusters])
            ohc_new = set([str(c) for c in ohc_clusters]) - set(df_sim.index)
            scipy_removed = set(df_sim.columns) - set([str(c) for c in scipy_clusters])
            scipy_new = set([str(c) for c in scipy_clusters]) - set(df_sim.columns)

            # Remove the old clusters from the similarity matrix
            df_sim.drop(scipy_removed, axis=1, inplace=True)
            df_sim.drop(ohc_removed, axis=0, inplace=True)

            # Compute the new similarities for the scipy_clusters
            for sn in scipy_new:
                sn_set = set(literal_eval(sn))
                df_sim[sn] = [jaccard_similarity(sn_set, set(literal_eval(c))) for c in df_sim.index]

            # Compute the new similarities for the ohc_clusters
            for ohcn in ohc_new:
                ohcn_set = set(literal_eval(ohcn))
                new_row = pd.Series(
                    data={c: jaccard_similarity(ohcn_set, set(literal_eval(c))) for c in df_sim.columns},
                    name=ohcn)

                df_sim = df_sim.append(new_row, ignore_index=False)

        sim1 = df_sim.max(0).mean()
        sim2 = df_sim.max(1).mean()
        similarity += (sim1 + sim2) / 2
    return similarity / len(ohc_hierarchy)


def tree_similarity(scipy_tree_matrix1, scipy_tree_matrix2):
    """
    Compute the similarity between two dendrogram represented as scipy tree matrices.
    -----
    :param scipy_tree_matrix1:  A scipy tree matrix
    :param scipy_tree_matrix2:  A scipy tree matrix
    :return:                    Return a value of similarity as a float between 0 and 1
    """
    similarity = 0
    df_sim = None
    n = scipy_tree_matrix1.shape[0]
    for i in range(n+1):
        # Clusters1
        scipy_cutree1 = cut_tree(scipy_tree_matrix1, n_clusters=i+1)
        scipy_clusters1 = [[] for _ in range(i+1)]
        for (j, v) in enumerate(scipy_cutree1):
            scipy_clusters1[v[0]].append(j)
        # Clusters2
        scipy_cutree2 = cut_tree(scipy_tree_matrix2, n_clusters=i+1)
        scipy_clusters2 = [[] for _ in range(i+1)]
        for (j, v) in enumerate(scipy_cutree2):
            scipy_clusters2[v[0]].append(j)
        # Initialization
        if df_sim is None:
            df_sim = pd.DataFrame(jaccard_sim_clusters(scipy_clusters1, scipy_clusters2),
                                  columns=[str(c) for c in scipy_clusters2])
            df_sim["index"] = [str(c) for c in scipy_clusters1]
            df_sim = df_sim.set_index("index")
            df_sim.index.name = None
        else:
            # Determine the changed in the 2 sets of scipy_clusters
            scipy_removed1 = set(df_sim.index) - set([str(c) for c in scipy_clusters1])
            scipy_new1 = set([str(c) for c in scipy_clusters1]) - set(df_sim.index)
            scipy_removed2 = set(df_sim.columns) - set([str(c) for c in scipy_clusters2])
            scipy_new2 = set([str(c) for c in scipy_clusters2]) - set(df_sim.columns)

            # Remove the old clusters from the similarity matrix
            df_sim.drop(scipy_removed2, axis=1, inplace=True)
            df_sim.drop(scipy_removed1, axis=0, inplace=True)

            # Compute the new similarities for the 2nd set of scipy_clusters
            for sn in scipy_new2:
                sn_set = set(literal_eval(sn))
                df_sim[sn] = [jaccard_similarity(sn_set, set(literal_eval(c))) for c in df_sim.index]

            # Compute the new similarities for the 1st set of scipy_clusters
            for sn in scipy_new1:
                sn_set = set(literal_eval(sn))
                new_row = pd.Series(
                    data={c: jaccard_similarity(sn_set, set(literal_eval(c))) for c in df_sim.columns},
                    name=sn)

                df_sim = df_sim.append(new_row, ignore_index=False)

        sim1 = df_sim.max(0).mean()
        sim2 = df_sim.max(1).mean()
        similarity += (sim1 + sim2) / 2
    return similarity/(n+1)


def ohc_similarity(ohc_hierarchy1, ohc_hierarchy2):
    """
    Compute the similarity between 2 OHC structures as an average of similarities of corresponding levels.
    -----
    :param ohc_hierarchy1:  An OHC object
    :param ohc_hierarchy2:  An OHC object
    :return:                Return a value of similarity as a float between 0 and 1
    """
    similarity = []
    levels1 = ohc_hierarchy1.levels()
    levels2 = ohc_hierarchy2.levels()
    depth1 = len(levels1)
    depth2 = len(levels2)
    i1 = depth1 - 1  # Position in the hierarchy 1
    i2 = depth2 - 1  # Position in the hierarchy 2
    nb_clust_old = -1  # Number of clusters of the previous levels if common, -1 otherwise
    while i1 >= 0 and i2 >= 0:
        clusters1 = levels1[i1].clusters()
        clusters2 = levels2[i2].clusters()
        nb_clust1 = len(clusters1)
        nb_clust2 = len(clusters2)

        ##########
        # Different number of clusters #

        if nb_clust1 < nb_clust2:
            # We take the sim max between sim(i1(nb_clust1), i2(nb_clusts2=nb_clust1+1))
            #                         and sim(i1(nb_clusts1), i2+1(nb_clust2-2=nb_clust1-1))
            sim_tmp = level_similarity(clusters1, clusters2)
            if i2 < depth2 - 1:
                clusters2_tmp = levels2[i2 + 1].clusters()
                sim_tmp = max(sim_tmp, level_similarity(clusters1, clusters2_tmp))

            similarity.append(sim_tmp)
            nb_clust_old = -1
            i1 -= 1
        elif nb_clust2 < nb_clust1:
            # We take the sim max between sim(i1(nb_clust1=nb_clust2+1), i2(nb_clusts2))
            # and sim(i1+1(nb_clusts1-2=nb_clust2-1), i2(nb_clust2))
            sim_tmp = level_similarity(clusters1, clusters2)
            if i1 < depth1 - 1:
                clusters1_tmp = levels1[i1 + 1].clusters()
                sim_tmp = max(sim_tmp, level_similarity(clusters1_tmp, clusters2))
            similarity.append(sim_tmp)
            nb_clust_old = -1
            i2 -= 1
        else:
            ##########
            # Same number of clusters #

            nb_clust = nb_clust1
            # Check if it is the first time we find this number of clusters of not
            if nb_clust != nb_clust_old:
                similarity.append(level_similarity(clusters1, clusters2))

            sims_tmp = dict()

            if i1 > 0:
                clusters1_tmp = levels1[i1 - 1].clusters()
                if len(clusters1_tmp) == nb_clust:
                    # If the level i1-1 has the same number of clusters than nb_clust
                    sims_tmp[(i1 - 1, i2)] = level_similarity(clusters1_tmp, clusters2)

                    if i2 > 0:
                        clusters2_tmp = levels2[i2 - 1].clusters()
                        if len(clusters2_tmp) == nb_clust:
                            # If both levels i1-1 and i2-1 has the same numbers of clusters than nb_clust
                            sims_tmp[(i1 - 1, i2 - 1)] = level_similarity(clusters1_tmp, clusters2_tmp)

            if i2 > 0:
                clusters2_tmp = levels2[i2 - 1].clusters()
                if len(clusters2_tmp) == nb_clust:
                    # If the level i2-1 has the same number of clusters than nb_clust
                    sims_tmp[(i1, i2 - 1)] = level_similarity(clusters1, clusters2_tmp)

            # Update nb_clust_old to the current value
            nb_clust_old = nb_clust
            # If necessary add a value to similarity and update i1 and i2
            if sims_tmp:
                arg_max = max(sims_tmp, key=sims_tmp.get)
                similarity.append(sims_tmp[arg_max])
                i1 = arg_max[0]
                i2 = arg_max[1]

            else:
                i1 -= 1
                i2 -= 1

    ##########
    # Different sizes of the vocab/number of clusters at the bottom of the hierarchies #

    # In the case where we stop because i2 = 0 and i1 >= 0
    clusters2 = levels2[0].clusters()
    while i1 >= 0:
        clusters1 = levels1[i1].clusters()
        similarity.append(level_similarity(clusters1, clusters2))
        i1 -= 1

    # In the case where we stop because i1 = 0 and i2 >= 0
    clusters1 = levels1[0].clusters()
    while i2 >= 0:
        clusters2 = levels2[i2].clusters()
        similarity.append(level_similarity(clusters1, clusters2))
        i2 -= 1

    return np.mean(similarity)
