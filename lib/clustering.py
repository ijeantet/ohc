"""
clustering.py:

Copyright (C) 2018-2020 Ian Jeantet <ian.jeantet@irisa.fr>
Copyright (C) 2018 Diego Cardenas Cabeza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import time
# External
import networkx as nx
import pandas as pd
import numpy as np
import ipywidgets as widgets
# Local files
import lib.utils as utils

# General configuration
logger = logging.getLogger()

###################
# Graph functions #
###################


def internal_degree(graph, nodes):
    """
    Compute for a given graph the number of internal links of a list of nodes.
    -----
    :param graph:   Undirected graph whom the nodes are a subset of its nodes
    :param nodes:   List of nodes of the graph
    :return:        Return the number of internal links
    """
    return graph.subgraph(nodes).number_of_edges()


def adjacent_nodes(graph, cluster):
    """
    Return the set of nodes that have at least a link with the given cluster in the graph.
    -----
    :param graph:   Undirected graph
    :param cluster: A Cluster object or a list of nodes of the graph
    :return:        A list of pair (node, nb_links) for each node linked to the cluster.
    """
    nodes = set(cluster)
    adj_nodes = dict()
    # For each node of cluster
    for n in nodes:
        # We look for the adjacent nodes of the graph that are not in the cluster
        for a in set(graph.adj[n]) - set(nodes):
            adj_nodes[a] = adj_nodes.get(a, 0) + 1
    return adj_nodes


def update_adjacent_nodes(graph, cluster, adj_nodes, new_nodes):
    """
    Update the set of nodes that have at least a link with the given cluster in the graph.
    -----
    :param graph:       Undirected graph
    :param cluster:     A Cluster object or a list of nodes of the graph
    :param adj_nodes:   Dictionary of pair (node, nb_links) already connected to the cluster.
    :param new_nodes:   New nodes to add to the cluster and to look at to update the set of adjacent node.
    :return:            A list of pair (node, nb_links) for each node linked to the cluster.
    """
    nodes = set(cluster) | set(new_nodes)
    new_adj_nodes = dict(adj_nodes)
    # For each node of cluster
    for n in new_nodes:
        # We look for the adjacent nodes of the graph that are not in the cluster
        for a in set(graph.adj[n]) - set(nodes):
            new_adj_nodes[a] = new_adj_nodes.get(a, 0) + 1
    return new_adj_nodes

#########################
# OHC object definition #
#########################


class Cluster(object):

    def __init__(self, nodes=None, nb_links=0, adj_nodes=None, children=None, graph=None):
        """
        Initializes a Cluster object.
        -----
        :param nodes:       Set of nodes of the cluster. If None is given, an empty set will be used
        :param nb_links:    Initial number of links between the nodes of the cluster
        :param graph:       Determine the number of internal links of the set of nodes from an undirected graph if given
        """
        if nodes is None:
            # Empty Cluster
            nodes = set()
            nb_links = 0
            children = []
            adj_nodes = []
            most_adj_links = 0
            most_adj_nb = 0
        else:
            nodes = set(nodes)
            if graph:
                # If a graph is given we determine the number of internal links from it
                nb_links = internal_degree(graph, nodes)
                adj_nodes = sorted(adjacent_nodes(graph, nodes).items(),
                                   key=lambda kv: kv[1], reverse=True)
            if adj_nodes:
                most_adj_links = adj_nodes[0][1]
                most_adj_nb = 0
                for adj in adj_nodes:
                    if adj[1] == most_adj_links:
                        most_adj_nb += 1
                    else:
                        break
            else:
                adj_nodes = []
                most_adj_links = 0
                most_adj_nb = 0

            if children is None:
                children = []
            else:
                children = list(children)
        self.__nodes = nodes                    # List of nodes of the cluster
        self.__nb_links = nb_links              # Number of internal links between the nodes
        self.__adj_nodes = adj_nodes            # list((adj_node, nb_links)) for each adjacent node
        self.__most_adj_links = most_adj_links  # Maximal number of links to an adjacent node
        self.__most_adj_nb = most_adj_nb        # Number of adjacent nodes that are the most linked
        self.__children = children              # FEATURE add a pointer to its child clusters

    def get_nodes(self):
        return list(self.__nodes)

    def to_labels(self, labels):
        return set(labels[c] for c in self.__nodes)

    def get_nb_links(self):
        return self.__nb_links

    def get_adj_nodes(self):
        return self.__adj_nodes

    def get_most_adj_links(self):
        return self.__most_adj_links

    def get_most_adj_nb(self):
        return self.__most_adj_nb

    def get_children(self):
        return self.__children

    def set_nodes(self, nodes):
        self.__nodes = nodes

    def set_nb_links(self, nb_links):
        self.__nb_links = nb_links

    def set_adj_nodes(self, adj_nodes):
        self.__adj_nodes = adj_nodes

    def update_adj_nodes(self, n):
        adj_nodes = dict(self.__adj_nodes)
        adj_nodes[n] = adj_nodes.get(n, 0) + 1
        self.__adj_nodes = sorted(adj_nodes.items(), key=lambda kv: kv[1], reverse=True)

    def set_most_adj_links(self, nb_links):
        self.__most_adj_links = nb_links

    def set_most_adj_nb(self, nb_nodes):
        self.__most_adj_nb = nb_nodes

    def increment_nb_links(self):
        self.__nb_links += 1
        return self

    def density(self):
        """
        Compute the density of the cluster
        -----
        :return:    The density as a float
        """
        if len(self.__nodes) <= 1:
            return 1.0
        else:
            return float(2 * self.__nb_links) / (len(self.__nodes) * (len(self.__nodes) - 1))

    def __iter__(self):
        return (c for c in self.get_nodes())

    def __str__(self):
        return str(self.__nodes)

    def __len__(self):
        return len(self.__nodes)

    def __eq__(self, other):
        return set(self) == set(other)

    def __hash__(self):
        return object.__hash__(self)

    def display(self):
        print("Cluster " + str(self.__nodes) + ":")
        print(" - " + str(self.__nb_links) + " link(s)")
        print(" - adjacent nodes = " + str(self.__adj_nodes))


class OHCLevel(object):

    def __init__(self, clust_li, d_min, d_max=None, density_min=None, density_max=None):
        """
        Initialize a level of a OHC object.
        -----
        :param clust_li:    List of Cluster objects
        :param d_min:       Distance minimal corresponding to the level
        :param d_max:       (Optional) Distance maximal corresponding to the level
        :param density_min: (Optional) List of densities of the Cluster objects corresponding to the minimal distance
        :param density_max: (Optional) List of densities of the Cluster objects corresponding to the maximal distance
        """
        if d_max is None:
            d_max = d_min
        if density_min is None:
            density_min = list()
        if density_max is None:
            density_max = density_min
        self.__d_min = d_min
        self.__d_max = d_max
        self.__clusters = list(clust_li)
        self.__density_min = list(density_min)
        self.__density_max = list(density_max)

    def d_min(self):
        return self.__d_min

    def d_max(self):
        return self.__d_max

    def set_d_min(self, d):
        self.__d_min = d

    def set_d_max(self, d):
        self.__d_max = d

    def clusters(self):
        return self.__clusters

    def remove_cluster(self, cluster):
        try:
            index = [set(c) for c in self.__clusters].index(set(cluster))
            del self.__clusters[index]
            del self.__density_min[index]
            del self.__density_max[index]
        except ValueError:
            raise Warning(str(cluster) + " not in the list of clusters!")

    def __len__(self):
        return len(self.__clusters)

    def density_min(self):
        return self.__density_min

    def density_max(self):
        return self.__density_max

    def set_density_min(self, density):
        self.__density_min = density

    def set_density_max(self, density):
        self.__density_max = density

    def display(self, labels=None):
        to_print = "distance: " + str(self.__d_min)
        if self.__d_max != self.__d_min:
            to_print += " -> " + str(self.__d_max)
        to_print += "; clusters: ["
        for clust, dens_min, dens_max in zip(self.__clusters, self.__density_min, self.__density_max):
            if labels is None:
                str_clust = str(set(clust))
            else:
                str_clust = str(set([labels[c] for c in clust]))
            to_print += str_clust + "(" + str(dens_min)
            if dens_min != dens_max:
                to_print += " -> " + str(dens_max)
            to_print += "), "
        to_print = to_print[:-2] + "]"
        print(to_print)

    def display_clusters(self):
        return str(self.__clusters)


class OHC(object):

    def __init__(self, level=None, name=None, labels=None, freqs=None):
        """
        For the integrity of the hierarchy we can set only one level during the initialisation.
        -----
        :param level:   (Optional) First level of the hierarchy to create
        :type level:    OHCLevel
        :param name:    Name of the hierarchy
        :param labels:  List of labels of the elements contained in the hierarchy
        """
        if level is None:
            self.__levels = list()
        else:
            self.__levels = list([level])
        if name is None:
            self.__name = "hierarchical_structure"
        else:
            self.__name = name
        if labels is None:
            if level:
                # FEAT. add labels according to the content of __levels and not only an integer
                self.__element_info = pd.DataFrame([str(i) for i in range(len(level))], columns=["label"])
                if freqs is None:
                    self.__element_info["frequency"] = 1  # Add frequency of 1 to every nodes
                else:
                    self.__element_info["frequency"] = list(freqs)
            else:
                self.__element_info = pd.DataFrame(columns=["label"])
        else:
            self.__element_info = pd.DataFrame(list(labels), columns=["label"])
            if freqs is None:
                self.__element_info["frequency"] = 1  # Add frequency of 1 to every nodes
            else:
                self.__element_info["frequency"] = list(freqs)

    # Level related methods #
    def levels(self, size=None):
        """
        Return the list of levels of the object optionally filtered to a given number of clusters.
        -----
        :param size:    Optional, requested size (number of clusters) of the levels to return.
        :return:        The list of levels of the object.
        """
        if size is None:
            return self.__levels
        else:
            i_min = 0
            i_max = len(self.__levels) - 1
            # Verify that the hierarchy is not empty
            if i_max < 0:
                raise ValueError('Empty hierarchy')
            level_tmp = self.__levels[i_min]
            # Verify that it is possible to have the required number of clusters in the hierarchy
            if len(level_tmp) < size:
                raise UserWarning('No level of the size ' + str(size) + ' in the hierarchy')
            else:
                levels = OHC(labels=self.labels(), freqs=self.frequencies())
                i = i_min
                while i <= i_max and len(self.__levels[i]) > size:
                    i += 1
                while i <= i_max and len(self.__levels[i]) == size:
                    levels.add_level(self.__levels[i])
                    i += 1
                return levels

    def nb_levels(self):
        return len(self.__levels)

    def __set_levels(self, ohc_levels):
        self.__levels = list(ohc_levels)

    def add_level(self, ohc_level):
        """
        Add a level to the hierarchy if the list of clusters is different from the last one present in the hierarchy.
        If the lists are the same, we update the distance max and the density max for the last level of clusters.
        -----
        :param ohc_level:   A OHCLevel object representing the level we want to add to the hierarchy.
        :type ohc_level:    OHCLevel
        :return:            Return the updated hierarchy.
        """
        if not self.__levels:
            # OHC is empty
            self.__levels = [ohc_level]
            if self.__element_info.empty:
                self.__element_info = pd.DataFrame([str(i) for i in range(len(ohc_level))], columns=["label"])
                self.__element_info["frequency"] = 1
        elif self.__levels[-1].clusters() == ohc_level.clusters():
            # Same list of cluster -> update d_max and density_max
            self.__levels[-1].set_d_max(ohc_level.d_max())
            self.__levels[-1].set_density_max(ohc_level.density_max())
        else:
            # Else add level
            self.__levels.append(ohc_level)

    def cut(self, size):
        """
        Cut the OHC object and return a coverage of the data with a given number of clusters.
        If several levels have the same size we compute the average persistence (d_death - d_birth) of their clusters
        and select the most persistent level.
        -----
        :param size:    Number of clusters
        :return:        An OHCLevel object containing the most persistent level of the correct size
        """
        i_min = 0
        i_max = len(self.__levels) - 1
        # Verify that the hierarchy is not empty
        if i_max < 0:
            raise ValueError('Empty hierarchy')

        # Look for the first level with a size not greater than the size we want by going up in the hierarchy
        i = i_max
        while i >= i_min and len(self.__levels[i]) < size:
            i -= 1

        # Get this first level
        i_level_max = i  # Level max of the correct size
        level_i = self.__levels[i]

        # We set size to the smallest value greater than size if not present in the hierarchy
        size = len(level_i)

        # Verify that it is possible to have the required number of clusters in the hierarchy
        # if len(level_i) != size:
        #    raise UserWarning('No level of the size ' + str(size) + ' in the hierarchy')

        # In the case there is only one level with the requested size
        if i-1 < i_min or len(self.__levels[i-1]) != size:
            return level_i

        # Initialize the output DataFrame with its content
        clusters_i = [set(ci) for ci in level_i.clusters()]
        candidate_clusters = pd.DataFrame({"cluster": clusters_i,
                                           "d_birth": level_i.d_min(),
                                           "d_death": level_i.d_max()})

        # Determine d_death for the first candidates by looking into the upper levels
        i_up = i_level_max+1
        clusters_common = clusters_i
        while i_up <= i_max:
            clusters_common_new = []
            level_up = self.__levels[i_up]
            clusters_up = [set(c_up) for c_up in level_up.clusters()]
            d_min_up = level_up.d_min()
            # For each cluster whom we still don't know the d_death
            for c in clusters_common:
                # we look for those who are in the upper level
                if c in clusters_up:
                    # We still don't know their d_death
                    clusters_common_new.append(c)
                else:
                    # We set their d_death to the d_min of the current upper level
                    # where they don't appear for the first time
                    candidate_clusters.at[
                        candidate_clusters.loc[candidate_clusters["cluster"] == c].index, "d_death"] = d_min_up
            # We stop the process if we didn't find any cluster of level_i in level_up (all the d_death are set)
            if not clusters_common_new:
                break
            clusters_common = clusters_common_new
            i_up += 1

        # Process all the lower levels of the same size
        i -= 1
        clusters_old = clusters_i
        while i >= i_min and len(self.__levels[i]) == size:
            level_i = self.__levels[i]
            clusters_i = [set(ci) for ci in level_i.clusters()]
            d_min = level_i.d_min()
            d_max = level_i.d_max()
            for c in clusters_i:
                # If c is not already a candidate we add it
                if c not in clusters_old:
                    candidate_clusters.loc[len(candidate_clusters)] = [c, d_min, d_max]
            for c in clusters_old:
                if c not in clusters_i:
                    candidate_clusters.at[
                        candidate_clusters.loc[candidate_clusters["cluster"] == c].index, "d_death"] = d_min
            clusters_old = clusters_i
            i -= 1
        i_level_min = i+1  # Level max of the correct size

        # Determine the d_birth of the last clusters by looking into the lower levels of the hierarchy
        i_low = i_level_min-1
        clusters_common = clusters_i
        while i_low >= i_min:
            clusters_common_new = []
            level_low = self.__levels[i_low]
            clusters_low = level_low.clusters()
            d_min_low = level_low.d_min()
            # For each cluster in the low level
            for c_low in clusters_low:
                c_low = set(c_low)
                # we look for those who are in the subset of clusters whom we still don't know the d_birth
                # (they were still present in the previous low level)
                if c_low in clusters_common:
                    # We update d_birth
                    candidate_clusters.at[candidate_clusters.loc[candidate_clusters["cluster"] == c_low].index, "d_birth"] = d_min_low
                    # And add c_low to the list of clusters of level_i that are still in the current low level
                    clusters_common_new.append(c_low)
            # We stop the process if we didn't find any cluster of level_i in level_low
            if not clusters_common_new:
                break
            clusters_common = clusters_common_new
            i_low -= 1

        # Compute the persistence (d_death - d_birth) of the candidates
        candidate_clusters["persistence"] = candidate_clusters["d_death"] - candidate_clusters["d_birth"]

        # Compute the average persistence of each level of the correct size
        level_persistence = []
        for i in range(i_level_min, i_level_max+1):
            level_i = self.__levels[i]
            clusters_i = [set(ci) for ci in level_i.clusters()]
            persistence_i = np.mean([float(candidate_clusters.loc[candidate_clusters["cluster"] == c]["persistence"]) for c in clusters_i])
            level_persistence.append(persistence_i)

        # Return a hierarchy containing the most persistent level
        i_best = i_level_min + np.argmax(level_persistence)
        return self.__levels[i_best]

    def __len__(self):
        return len(self.__levels)

    # Label related methods #

    # def labels(self):
    #     return self.__labels

    def labels(self, cluster=None, most_freq=None):
        if cluster is None:
            return list(self.__element_info["label"])
        else:
            if most_freq and most_freq > 0:
                clust_elements = self.__element_info.iloc[cluster]
                sorted_elements = clust_elements.sort_values(by=['frequency'], ascending=False)
                return list(sorted_elements[:most_freq]["label"])
            else:
                return list(self.__element_info.iloc[cluster]["label"])

    def most_freq(self, cluster_labels=None, k=None):
        if not k:
            if cluster_labels:
                return cluster_labels
            else:
                return self.__element_info["label"]
        else:
            clust_elements = self.__element_info
            if cluster_labels:
                clust_elements = clust_elements[clust_elements["label"].isin(cluster_labels)]
            sorted_elements = clust_elements.sort_values(by=['frequency'], ascending=False)
            return list(sorted_elements[:k]["label"])
    # Frequency related methods #

    def frequencies(self):
        return list(self.__element_info["frequency"])

    # Name related methods #

    def name(self):
        return self.__name

    def rename(self, name):
        self.__name = name

    # Other methods #

    def display(self):
        print("--- OHC object ---")
        print(self.__name)
        for level in self.__levels:
            level.display(labels=self.labels())
        print("------------------")

    def top_k(self, k=-1):
        """
        Return the top of the hierarchy up to a maximal number of clusters equals to k.
        -----
        :param k:   Maximal limit for the number of the clusters. Can be less if k is greater than the number of leaves
                    of if there is not level with k clusters in the hierarchy.
        :return:    A OHC object containing the top k hierarchy.
        """
        n = len(self)
        levels = OHC(name=self.__name + ".top_" + str(k), labels=self.labels(), freqs=self.frequencies())
        if k <= 0 or k >= len(self.__levels[0]):
            levels.rename(self.__name)
            levels.__set_levels(self.__levels)
        else:
            i_min = 0
            i_max = n
            i = int((i_min + i_max) / 2)
            while not (len(self.__levels[i]) == k and len(self.__levels[i-1]) > k):
                if len(self.__levels[i]) > k:
                    i_min = i
                else:
                    i_max = i
                i = int((i_min + i_max) / 2)
                # If after update i == i_min then k is not present in the hierarchy so we cut at k-1
                if i == i_min:
                    i = i_max
                    break
            levels.__set_levels(self.__levels[i:])
        return levels

    def detect_cluster_level(self, clust, i_min=0, i_max=None):
        """
        Look for the first occurrence of the smallest cluster that contains clust. Return the index of the
        corresponding level and this cluster.
        -----
        :param clust:       Cluster to look for (as a list, a set of elements or a Cluster object ).
        :param i_min:       Minimal range of the hierarchy where to look (by default this value is set to 0).
        :param i_max:       Maximal range of the hierarchy where to look.
                            (by default this value is set to the length of the hierarchy minus 1).
        :return:            Return the smallest cluster containing clust and the corresponding level index or
                            respectively -1 and None if the clust is not found.
        """
        # Check parameters
        if i_max is None:
            i_max = len(self) - 1
        s_clust = set(clust)

        # Basic case
        if i_min == i_max:
            for c in self.__levels[i_min].clusters():
                if s_clust.issubset(set(c)):
                    return i_min, c
            return -1, None

        # General case
        else:
            i = int((i_min + i_max) / 2)

            if i == i_min:  # ie i_min == i_max - 1
                for c in self.__levels[i_min].clusters():
                    if s_clust.issubset(set(c)):
                        return i_min, c
                for c in self.__levels[i_max].clusters():
                    if s_clust.issubset(set(c)):
                        return i_max, c
                return -1, None
            else:
                # For the current level
                for c in self.__levels[i].clusters():
                    # If we find a cluster whom clust is a subset we need to search in the lower part of the hierarchy
                    if s_clust.issubset(set(c)):
                        return self.detect_cluster_level(clust, i_min, i)
                # Otherwise in the upper part
                return self.detect_cluster_level(clust, i, i_max)

    def detect_branch(self, clust):
        """
        Detect the clusters included and including clust in the hierarchy. This constitutes the "branch" in which clust
        appears.
        -----
        :param clust:       Cluster to look for (as a list, set of elements or Cluster object).
        :return:            Return the branch of the hierarchy in which clust appears.
        """
        s_clust = set(clust)
        # Init the branch as an empty hierarchy
        branch = OHC(name="branch_" + self.__name, labels=self.labels(), freqs=self.frequencies())
        # For each level we add a level to the branch
        for level_tmp in self.__levels:
            # Determine the indices of the sub and super clusters
            indices_tmp = [i for i, c in enumerate(level_tmp.clusters())
                           if set(c).issubset(s_clust) or s_clust.issubset(set(c))]
            branch.add_level(OHCLevel(clust_li=[level_tmp.clusters()[j] for j in indices_tmp],
                                      d_min=level_tmp.d_min(), d_max=level_tmp.d_max(),
                                      density_min=[level_tmp.density_min()[j] for j in indices_tmp],
                                      density_max=[level_tmp.density_max()[j] for j in indices_tmp]
                                      )
                             )
        return branch

    def detect_sub_hierarchy(self, clust):
        """
        Look for the first occurrence of the smallest cluster that contains clust in the hierarchy and then determine
        the upper branch in which this cluster appears as well as the sub hierarchy starting from this cluster.
        -----
        :param clust:       Cluster to look for (as a list, set of elements or Cluster object).
        :return:            Return a part of the hierarchy containing the upper branch and the sub hierarchy of clust.
        """
        s_clust = set(clust)
        i_clust, h_clust = self.detect_cluster_level(clust)
        if i_clust >= 0:
            # Process cluster level
            level_clust = self.__levels[i_clust]
            # Detect clusters
            indices_tmp = [i for i, c in enumerate(level_clust.clusters())
                           if set(c).issubset(s_clust) or s_clust.issubset(set(c))]
            # Init sub hierarchy
            branch = [OHCLevel(clust_li=[level_clust.clusters()[j] for j in indices_tmp],
                               d_min=level_clust.d_min(), d_max=level_clust.d_max(),
                               density_min=[level_clust.density_min()[j] for j in indices_tmp],
                               density_max=[level_clust.density_max()[j] for j in indices_tmp]
                               )
                      ]

            # First part: detect sub clusters of h_clust
            for i in range(1, i_clust + 1):
                h_tmp = self.__levels[i_clust - i]
                h_old = branch[-1]
                clusters_tmp = []
                indices_tmp = []
                for i_tmp, c_tmp in enumerate(h_tmp.clusters()):
                    for c_old in h_old.clusters():
                        if set(c_tmp).issubset(set(c_old)):
                            clusters_tmp.append(c_tmp)
                            indices_tmp.append(i_tmp)

                # If we obtain the same set of clusters than the previous level we update the minimal values of distance
                # and density (we need to slice the density list to keep only those of the subset of clusters)
                if h_old.clusters() == clusters_tmp:
                    branch[-1].set_d_min(h_tmp.d_min())
                    branch[-1].set_density_min([h_tmp.density_min()[j] for j in indices_tmp])
                # Else we add a new level (here we need to slice both density lists)
                else:
                    branch.append(OHCLevel(clust_li=clusters_tmp, d_min=h_tmp.d_min(), d_max=h_tmp.d_max(),
                                           density_min=[h_tmp.density_min()[j] for j in indices_tmp],
                                           density_max=[h_tmp.density_max()[j] for j in indices_tmp]
                                           )
                                  )
            branch.reverse()

            # Second part: detect super clusters of h_clust
            for h_tmp in self.__levels[i_clust + 1:]:
                clusters_tmp = []
                indices_tmp = []
                for i, c in enumerate(h_tmp.clusters()):
                    if s_clust.issubset(set(c)):
                        clusters_tmp.append(c)
                        indices_tmp.append(i)

                if branch[-1].clusters() == clusters_tmp:
                    branch[-1].set_d_max(h_tmp.d_max())
                    branch[-1].set_density_max([h_tmp.density_max()[j] for j in indices_tmp])
                else:
                    branch.append(OHCLevel(clust_li=clusters_tmp, d_min=h_tmp.d_min(), d_max=h_tmp.d_max(),
                                           density_min=[h_tmp.density_min()[j] for j in indices_tmp],
                                           density_max=[h_tmp.density_max()[j] for j in indices_tmp]
                                           )
                                  )
            sub_hierarchy = OHC(name="sub_" + self.__name, labels=self.labels(), freqs=self.frequencies())
            sub_hierarchy.__set_levels(branch)
            return sub_hierarchy
        else:
            raise ValueError(str(s_clust) + " does not appear in the hierarchy.")

    def issubhierarchy(self, hierarchy):
        """
        Return True if the OHC object is a sub hierarchy of the hierarchy given in parameter
        -----
        :param hierarchy:   An OHC object
        :return:            True if self is in hierarchy. False otherwise
        """

        levels1 = hierarchy.levels()
        levels2 = self.__levels
        depth1 = len(levels1)
        depth2 = len(levels2)
        i1 = depth1 - 1  # Position in the hierarchy 1
        i2 = depth2 - 1  # Position in the hierarchy 2
        while i1 >= 0 and i2 >= 0:
            clusters1 = levels1[i1].clusters()
            clusters2 = levels2[i2].clusters()
            nb_clust1 = len(clusters1)
            nb_clust2 = len(clusters2)

            ##########
            # Different number of clusters #
            if nb_clust1 > nb_clust2:
                return False
            elif nb_clust1 < nb_clust2:
                i1 -= 1

            ##########
            # Same number of clusters #
            else:
                if set([frozenset(c1) for c1 in clusters1]) == set([frozenset(c2) for c2 in clusters2]):
                    i1 -= 1
                    i2 -= 1
                else:
                    i1 -= 1

        if i2 > 0:
            return False
        else:
            return True

#################
# OHC functions #
#################


def hierarchical_clustering_with_overlapping(data, merging_criterion=0.1, batch_size=1, normalize=True,
                                             preprocessed_hierarchy=None, name=None, labels=None, freqs=None,
                                             wid_container=None, time_limit=None, logs=None):
    """
    Perform an agglomerative fuzzy clustering on a given distance matrix. This algorithm works by looking at the
    distances in an increasing order.
    -----
    :param data:                    Either a distance matrix or a sorted list of tuples
    :param merging_criterion:       Merging criterion that will allow 2 clusters to merge partially or completely during
                                    the process
    :param batch_size:              Size of the batch of distances that we need to take in count before looking for any
                                    cluster modifications
    :param normalize:               Whether or not the distances should be normalized
    :param preprocessed_hierarchy:  A dictionary containing the necessary elements to initialize the hierarchical
                                    clustering to a specific state (graph, clusters, hierarchy and starting_distance)
    :param name:                    Name of the hierarchy
    :param labels:                  List of labels of the elements contained in the hierarchy (leave elements)
    :param freqs:                   List of frequencies of the elements contained in the hierarchy (leave elements)
    :param wid_container:           Container where to display the progresses and results. Default to None = standard
                                    output
    :param time_limit:              Stop the process if the execution time exceed this value (in seconds)
    :param logs:                    Log some information during the processing of the algo
    :return:                        Return the built hierarchy which is a list of levels where each level is a list of
                                    clusters associated to an interval of distances
    """

    # Init logs #
    #############

    if logs is None:
        logs = dict()

    time_start = time.process_time()
    ts = time.gmtime()
    log_file_name = "hierarchy_time_" + time.strftime("%s", ts) + ".csv"
    logs["batch_size"] = batch_size

    # Constants and variables #
    ###########################

    # For log and widget purpose
    every = 1000

    # Counter variables
    nb_tuples = 0
    nb_impacted_clusters = 0
    nb_batchs = 0

    # For the hierarchy computation
    d_old = 0
    d = 0
    d_min = 0
    d_max = 0
    current_batch = 0  # Will represent the number of different distances we saw by processing the tuples one by one
    impacted_clusters = set()

    # Distance matrix pre-processing #
    ##################################

    try:
        nb_vectors = data.shape[0]
        # From the distance matrix to a sorted list of tuples
        li = utils.matrix_to_tuples(data, triangle=True)
        sorted_li = sorted(li, key=utils.tuple_distance)
    except AttributeError:
        # If it's not a matrix we suppose that a list of tuples was given
        sorted_li = data
        nb_vectors = int((1+(8*len(sorted_li) + 1)**.5)/2)

    if len(sorted_li) > 0 and normalize:
        # min_dist = get_distance(sorted_li[0])
        min_dist = 0
        norm_factor = utils.tuple_distance(sorted_li[-1]) - min_dist
        if norm_factor != 1:
            sorted_li = [(a, b, float(d-min_dist)/norm_factor) for (a, b, d) in sorted_li]

    # Object initialisation #
    #########################

    if name is None:
        if preprocessed_hierarchy:
            hierarchy_name = preprocessed_hierarchy["hierarchy"].name()
        else:
            hierarchy_name = "hierarchical_structure"
    else:
        hierarchy_name = name
    hierarchy_name += ".merge_" + str(merging_criterion).replace('.', '_') + ".batch_" + str(batch_size) + ".ohc"

    if preprocessed_hierarchy:
        # Init graph
        distance_graph = preprocessed_hierarchy["graph"]
        # Init cluster list
        clusters = preprocessed_hierarchy["clusters"]
        # Init hierarchy
        hierarchy = preprocessed_hierarchy["hierarchy"]
        hierarchy.rename(hierarchy_name)
        # Cut the list of tuples
        starting_distance = preprocessed_hierarchy["distance"]

        while sorted_li and utils.tuple_distance(sorted_li[0]) <= starting_distance:
            sorted_li.pop(0)
        # Update hierarchy computation variables
        d_old = starting_distance
        d = starting_distance
        d_min = starting_distance
        d_max = starting_distance

    else:
        # Init graph
        distance_graph = nx.Graph(name='hierarchy_generation_graph')
        distance_graph.add_nodes_from([i for i in range(nb_vectors)])
        # Init cluster list
        clusters = [Cluster(nodes=[n]) for n in distance_graph.nodes]
        # Init hierarchy
        hierarchy = OHC(OHCLevel([c.get_nodes() for c in clusters], d_min=d_min,
                                 density_min=[c.density() for c in clusters]),
                        name=hierarchy_name, labels=labels, freqs=freqs)

    nb_clusters = len(clusters)

    # Init widgets #
    ################

    if wid_container:
        wid_time = widgets.Button(description="0s")
        wid_total_clusters = widgets.Button(description=str(nb_clusters))
        wid_current_clusters = widgets.Button(description=str(len(clusters)))
        wid_hierarchy = widgets.Button(description="1")
        wid_log = widgets.HTML(value="")
        wid_progress = widgets.VBox([widgets.HBox([widgets.Button(description="Time"), wid_time]),
                                     widgets.HBox([widgets.Button(description="Total clusters"), wid_total_clusters]),
                                     widgets.HBox([widgets.Button(description="Current clusters"), wid_current_clusters]),
                                     widgets.HBox([widgets.Button(description="Number of levels"), wid_hierarchy]),
                                     wid_log
                                     ])

    # Hierarchy computation #
    #########################

    # Add links
    for tu in utils.log_progress(sorted_li, every=every, name='Tuples', wid_container=wid_container):
        nb_tuples += 1

        d = utils.tuple_distance(tu)

        # When we reach the batch size we need to be sure that we took in count all the links for the last distance
        if current_batch >= batch_size and d != d_old:

            # Batch statistics
            nb_batchs += 1
            nb_impacted_clusters += len(impacted_clusters)

            # Reinitialize the variables
            current_batch = 0

            # We try to grow the impacted clusters by adding the most linked adjacent nodes
            # if they keep the same density
            # Clusters to delete from the list of clusters as they are merged with another one
            modified_cluster_indices = set()
            # New clusters we will compute
            new_clusters = set()
            for i_imp in list(impacted_clusters):

                # Info of the impacted cluster
                clust_tmp = clusters[i_imp]     # Cluster object
                nodes_tmp = set(clust_tmp)      # Set of nodes

                # Info of the impacted cluster
                nb_links_tmp = clust_tmp.get_nb_links()             # Nb of internal links of the cluster
                clust_density_tmp = clust_tmp.density()             # Internal density of the cluster
                adj_nodes_tmp = clust_tmp.get_adj_nodes().copy()    # list((adj_node, nb_links))

                # We look for a new cluster only if the maximal number of links to a neighbour node increased
                # or if the number of most adjacent nodes increased
                # How about when a link is added between most adjacent nodes? The density of the new cluster candidate
                # would increase in this case!
                # if adj_nodes_tmp and (adj_nodes_tmp[0][1] > most_adj_links_tmp or most_adj_nb > most_adj_nb_tmp):

                while adj_nodes_tmp:
                    # Copy of the adjacent nodes in case they don't change
                    adj_nodes_save = adj_nodes_tmp.copy()

                    # We look for the set of nodes that are the most linked with clust_tmp
                    n_tmp = adj_nodes_tmp.pop(0)    # Current adjacent node
                    adj_nb_links = n_tmp[1]         # Associated adjacent links
                    nodes_to_add = [n_tmp[0]]
                    while adj_nodes_tmp:
                        n_tmp = adj_nodes_tmp.pop(0)
                        if n_tmp[1] == adj_nb_links:
                            nodes_to_add.append(n_tmp[0])
                        else:
                            adj_nodes_tmp.insert(0, n_tmp)
                            break

                    # New cluster candidate
                    new_clust_nb_links = nb_links_tmp + len(nodes_to_add) * adj_nb_links \
                                         + internal_degree(distance_graph, nodes_to_add)
                    new_nb_nodes = len(nodes_tmp) + len(nodes_to_add)
                    new_clust_density = float(2 * new_clust_nb_links) / (new_nb_nodes * (new_nb_nodes - 1))

                    # Validation?
                    # FEATURE Always compare the new density to the density of the impacted cluster even if it
                    #  already grew?
                    if abs(clust_density_tmp - new_clust_density) <= merging_criterion:

                        # If we added nodes to the cluster we start the process again
                        adj_nodes_tmp = sorted(
                            update_adjacent_nodes(distance_graph, nodes_tmp, adj_nodes_tmp, nodes_to_add).items(),
                            key=lambda kv: kv[1], reverse=True)

                        # Update the temporary cluster with the new nodes
                        nodes_tmp.update(nodes_to_add)
                        nb_links_tmp = new_clust_nb_links
                        clust_density_tmp = float(2 * nb_links_tmp) / (len(nodes_tmp) * (len(nodes_tmp) - 1))

                        # Else we break the loop
                    else:
                        # And we restore the adj_nodes to the save
                        adj_nodes_tmp = adj_nodes_save
                        break

                # We create a new cluster if the set of nodes has changed
                if nodes_tmp != set(clust_tmp):
                    modified_cluster_indices.add(i_imp)

                    # Look if the new created cluster is not a subset of another new cluster
                    sub = False
                    for c in new_clusters:
                        if nodes_tmp.issubset(set(c)):
                            sub = True
                            break
                    # If not we will add it to the list of new clusters
                    if not sub:
                        # Remove the new clusters included in the current new cluster
                        new_clusters = set(c for c in new_clusters if not set(c).issubset(nodes_tmp))
                        # Add the new cluster to the list of new clusters
                        # FEATURE add the children to the new cluster
                        new_clusters.add(Cluster(nodes=nodes_tmp, nb_links=nb_links_tmp, adj_nodes=adj_nodes_tmp))

            # Clean the list of clusters
            # Remove the sub clusters from the list
            for index in sorted(list(modified_cluster_indices), reverse=True):
                # print("- " + str(clusters[index].get_nodes()))
                del clusters[index]
            # Look if a non modified clusters is a subset of a new_clusters
            for nc in new_clusters:
                clusters = [c for c in clusters if not set(c).issubset(set(nc))]

            # Add the new clusters to the list of clusters
            clusters.extend(new_clusters)
            nb_clusters += len(new_clusters)

            # Update the hierarchy
            hierarchy.add_level(OHCLevel(clust_li=[c.get_nodes() for c in clusters],
                                         d_min=d_min, d_max=d_max,
                                         density_min=[c.density() for c in clusters]
                                         )
                                )
            # If we have only one cluster, we can stop to add links in the graph, nothing will change anymore
            if len(clusters) == 1:
                hierarchy.add_level(OHCLevel(clust_li=[c.get_nodes() for c in clusters],
                                             d_min=d_min, d_max=utils.tuple_distance(sorted_li[-1]),
                                             density_min=[c.get_nodes() for c in clusters],
                                             density_max=[1]
                                             )
                                    )
                break

        if current_batch == 0:
            d_min = d
            impacted_clusters = set()

        # Update graph
        u = tu[0]
        v = tu[1]
        w = tu[2]
        distance_graph.add_edge(u, v, weight=w)

        # Determine the impacted clusters
        # WARNING We should also add clusters that have the 2 nodes in their most adjacent list as the density of the
        # new cluster candidate is increased
        for i, c in enumerate(clusters):
            if u in c.get_nodes():
                impacted_clusters.add(i)
                if v in c.get_nodes():
                    c.set_nb_links(c.get_nb_links() + 1)
                else:
                    c.update_adj_nodes(v)
            elif v in c.get_nodes():
                impacted_clusters.add(i)
                c.update_adj_nodes(u)

        # We increment i that represents the number of different seen distances for the current batch
        if d != d_old:
            current_batch += 1
            d_old = d
            if current_batch == batch_size:
                d_max = d

        # Logs
        if nb_tuples == 1 and wid_container:
            widget_content = list(wid_container.children)
            widget_content.append(wid_progress)
            wid_container.children = widget_content

        if not nb_tuples % every:
            log_data = dict()
            log_data["nb_tuples"] = nb_tuples
            log_data["time"] = time.process_time() - time_start
            log_data["total_clusters"] = nb_clusters
            log_data["nb_clusters"] = len(clusters)
            log_data["hierarchy_size"] = len(hierarchy)
            utils.save_log(log_file_name, log_data)

            if wid_container:
                wid_time.description = "%.2f" % round(time.process_time() - time_start, 2) + "s"
                wid_total_clusters.description = str(nb_clusters)
                wid_current_clusters.description = str(len(clusters))
                wid_hierarchy.description = str(len(hierarchy))
            else:
                print(str(nb_tuples) + " tuples processed in %.2f" % round(time.process_time() - time_start, 2) + "s")
                print("-> " + str(nb_clusters) + " clusters in total")
                print("-> " + str(len(clusters)) + " clusters in the "
                      + str(len(hierarchy)) + "th level of the hierarchy")

            if time_limit and log_data["time"] > time_limit:
                print("TIME LIMIT EXCEEDED (limit of " + str(time_limit) + "s)")
                break

    # Ending hierarchy #
    ####################

    # If we didn't break the loop because we reached a unique cluster we need to add a last level to the hierarchy
    # for the last incomplete batch
    if current_batch != 0:

        # Batch statistics
        nb_batchs += 1
        nb_impacted_clusters += len(impacted_clusters)

        d_max = d

        # We try to grow the impacted clusters by adding the most linked adjacent nodes
        # if they keep the same density
        # Clusters to delete from the list of clusters as they are merged with another one
        modified_cluster_indices = set()
        # New clusters we will compute
        new_clusters = set()
        for i_imp in list(impacted_clusters):

            # Info of the impacted cluster
            clust_tmp = clusters[i_imp]  # Cluster object
            nodes_tmp = set(clust_tmp)  # Set of nodes

            # We look if the current cluster is a subset of a already computed new cluster
            # Not a good idea if we add lot of links that have the same distance (ie at the same time)
            # sub = False
            # for c in new_clusters:
            #     if nodes_tmp.issubset(set(c)):
            #         modified_cluster_indices.add(i_imp)
            #         nb_merges += 1
            #         sub = True
            #         # FEATURE update c children
            #         break

            # We process it only if it's not the case
            # if not sub:

            # Info of the impacted cluster
            nb_links_tmp = clust_tmp.get_nb_links()  # Nb of internal links of the cluster
            clust_density_tmp = clust_tmp.density()  # Internal density of the cluster
            adj_nodes_tmp = clust_tmp.get_adj_nodes().copy()  # list((adj_node, nb_links))

            # We look for a new cluster only if the maximal number of links to a neighbour node increased
            # or if the number of most adjacent nodes increased
            # How about when a link is added between most adjacent nodes? The density of the new cluster candidate
            # would increase in this case!
            # if adj_nodes_tmp and (adj_nodes_tmp[0][1] > most_adj_links_tmp or most_adj_nb > most_adj_nb_tmp):

            while adj_nodes_tmp:
                # Copy of the adjacent nodes in case they don't change
                adj_nodes_save = adj_nodes_tmp.copy()

                # We look for the set of nodes that are the most linked with clust_tmp
                n_tmp = adj_nodes_tmp.pop(0)  # Current adjacent node
                adj_nb_links = n_tmp[1]  # Associated adjacent links
                nodes_to_add = [n_tmp[0]]
                while adj_nodes_tmp:
                    n_tmp = adj_nodes_tmp.pop(0)
                    if n_tmp[1] == adj_nb_links:
                        nodes_to_add.append(n_tmp[0])
                    else:
                        adj_nodes_tmp.insert(0, n_tmp)
                        break

                # New cluster candidate
                new_clust_nb_links = nb_links_tmp + len(nodes_to_add) * adj_nb_links \
                                     + internal_degree(distance_graph, nodes_to_add)
                new_nb_nodes = len(nodes_tmp) + len(nodes_to_add)
                new_clust_density = float(2 * new_clust_nb_links) / (new_nb_nodes * (new_nb_nodes - 1))

                # Validation?
                # FEATURE Always compare the new density to the density of the impacted cluster even if it
                #  already grew?
                if abs(clust_density_tmp - new_clust_density) <= merging_criterion:

                    # If we added nodes to the cluster we start the process again
                    adj_nodes_tmp = sorted(
                        update_adjacent_nodes(distance_graph, nodes_tmp, adj_nodes_tmp, nodes_to_add).items(),
                        key=lambda kv: kv[1], reverse=True)

                    # Update the temporary cluster with the new nodes
                    nodes_tmp.update(nodes_to_add)
                    nb_links_tmp = new_clust_nb_links
                    clust_density_tmp = float(2 * nb_links_tmp) / (len(nodes_tmp) * (len(nodes_tmp) - 1))

                    # Else we break the loop
                else:
                    # And we restore the adj_nodes to the save
                    adj_nodes_tmp = adj_nodes_save
                    break

            # We create a new cluster if the set of nodes has changed
            if nodes_tmp != set(clust_tmp):
                modified_cluster_indices.add(i_imp)

                # Look if the new created cluster is not a subset of another new cluster
                sub = False
                for c in new_clusters:
                    if nodes_tmp.issubset(set(c)):
                        sub = True
                        break
                # If not we will add it to the list of new clusters
                if not sub:
                    # Remove the new clusters included in the current new cluster
                    new_clusters = set(c for c in new_clusters if not set(c).issubset(nodes_tmp))
                    # Add the new cluster to the list of new clusters
                    # FEATURE add the children to the new cluster
                    new_clusters.add(Cluster(nodes=nodes_tmp, nb_links=nb_links_tmp, adj_nodes=adj_nodes_tmp))

        # Clean the list of clusters
        # Remove the sub clusters from the list
        for index in sorted(list(modified_cluster_indices), reverse=True):
            # print("- " + str(clusters[index].get_nodes()))
            del clusters[index]
        # Look if a non modified clusters is a subset of a new_clusters
        for nc in new_clusters:
            clusters = [c for c in clusters if not set(c).issubset(set(nc))]

        # Add the new clusters to the list of clusters
        clusters.extend(new_clusters)
        nb_clusters += len(new_clusters)

        # Update the hierarchy
        hierarchy.add_level(OHCLevel(clust_li=[c.get_nodes() for c in clusters],
                                     d_min=d_min, d_max=d_max,
                                     density_min=[c.density() for c in clusters]
                                     )
                            )

    # Ending logs #
    ###############

    time_stop = time.process_time()
    log_data = dict()
    log_data["nb_tuples"] = nb_tuples
    log_data["time"] = time_stop - time_start
    log_data["total_clusters"] = nb_clusters
    log_data["nb_clusters"] = len(clusters)
    log_data["hierarchy_size"] = len(hierarchy)
    utils.save_log(log_file_name, log_data)

    if wid_container:
        wid_time.description = "%.2f" % round(time.process_time() - time_start, 2) + "s"
        wid_total_clusters.description = str(nb_clusters)
        wid_current_clusters.description = str(len(clusters))
        wid_hierarchy.description = str(len(hierarchy))

        wid_log_content = ""
        if nb_batchs > 0:
            wid_log_content += "Average of " + str(nb_impacted_clusters / nb_batchs) + " impacted clusters per batch<br>"
        else:
            wid_log_content += "No batch for this process<br>"
        wid_log_content += "Logs saved in " + log_file_name
        wid_log.value = wid_log_content
    else:
        print(str(nb_tuples) + " tuples processed in %.2f" % round(time.process_time() - time_start, 2) + "s")
        print("-> " + str(nb_clusters) + " clusters in total")
        print("-> " + str(len(clusters)) + " clusters in the "
              + str(len(hierarchy)) + "th level of the hierarchy")
        if nb_batchs > 0:
            print("Average of " + str(nb_impacted_clusters / nb_batchs) + " impacted clusters per batch")
        else:
            print("No batch for this process")
        print("OHC logs saved in " + log_file_name)

    logs["nb_tuples"] = nb_tuples
    if time_limit and time_stop - time_start >= time_limit:
        logs["time"] = -1
    else:
        logs["time"] = time_stop - time_start
    if nb_batchs > 0:
        logs["avg_impacted_clusters"] = nb_impacted_clusters / nb_batchs
    else:
        logs["avg_impacted_clusters"] = 1

    return hierarchy


def preprocess_hierarchy(data, d_threshold, normalize=True, name=None, labels=None, freqs=None, wid_container=None):
    """
    Preprocess the hierarchy by computing the cliques on the graph containing certain links according to d_threshold.
    -----
    :param data:            Either a distance matrix or a sorted list of tuples
    :param d_threshold:     Fraction of links/distances to take in count (0 => 0 distances, 1 => all distances)
    :param normalize:       Whether of not dealing with normalized distances
    :param name:            Name of the hierarchy
    :param labels:          List of labels of the elements contained in the hierarchy (leave elements)
    :param freqs:           List of frequencies of the elements contained in the hierarchy (leave elements)
    :param wid_container:   Container where to display the progresses and results. Default to None = standard output
    :return:                Return an object usable by the hierarchy computation algorithm
                            (will init the process to this returned state)
    """

    time_start = time.process_time()

    # # Get info on the distance matrix
    # mat_size = distance_matrix.shape[0]

    # Init ph_graph
    if name is None:
        prep_name = "hierarchical_structure"
    else:
        prep_name = name
    prep_name += ".preprocessed_" + str(d_threshold).replace('.', '_')
    ph_graph = nx.Graph(name='preprocess_hierarchy_graph')

    # Distance matrix pre-processing #
    ##################################

    try:
        nb_vectors = data.shape[0]
        # From the distance matrix to a sorted list of tuples
        li = utils.matrix_to_tuples(data, triangle=True)
        sorted_li = sorted(li, key=utils.tuple_distance)
    except AttributeError:
        # If it's not a matrix we suppose that a list of tuples was given
        sorted_li = data
        nb_vectors = int((1+(8*len(sorted_li) + 1)**.5)/2)

    if len(sorted_li) > 0 and normalize:
        # min_dist = get_distance(sorted_li[0])
        min_dist = 0
        norm_factor = utils.tuple_distance(sorted_li[-1]) - min_dist
        if norm_factor != 1:
            sorted_li = [(a, b, float(d-min_dist)/norm_factor) for (a, b, d) in sorted_li]

    # Add the nodes to the graph
    ph_graph.add_nodes_from([i for i in range(nb_vectors)])

    # Add the edges to the graph according to d_threshold
    max_added_distance = 0
    while sorted_li and utils.tuple_distance(sorted_li[0]) <= d_threshold:
        (i, j, max_added_distance) = sorted_li.pop(0)
        ph_graph.add_edge(i, j, weight=max_added_distance)

    # Compute the cliques of ph_graph
    cliques = nx.find_cliques(ph_graph)
    cliques = list(cliques)

    # From cliques to cluster objects
    clusters = [Cluster(nodes=c, graph=ph_graph) for c in list(cliques)]

    # Init hierarchy
    hierarchy = OHC(OHCLevel([c.get_nodes() for c in clusters], d_min=0, d_max=max_added_distance,
                             density_min=[c.density() for c in clusters]), name=prep_name, labels=labels, freqs=freqs)

    utils.to_print("Hierarchy preprocessed in " + str(time.process_time() - time_start) + "s -> " + str(len(clusters))
                   + " cliques found", wid_container)

    return {"graph": ph_graph, "clusters": clusters, "hierarchy": hierarchy, "data": sorted_li,
            "distance": max_added_distance}
